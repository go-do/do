package do

import (
	// "fmt"
	// "os"
	"testing"
)

func Test_CssFile_Create(t *testing.T) {
	CssFile("test",
		Css(".row",
			SMap{
				"padding": "5px",
			},
			Css(".even",
				SMap{
					"background": "#EEE",
				},
			),
			Css(".odd",
				SMap{
					"background": "#DDD",
				},
			),
		),
	)

	// os.RemoveAll(Conf.Path["static"])
}
