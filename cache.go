package do

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	cacheClean = iota
	cacheHit
	cacheHitDelete
	cacheHitSet
	cachePrune
	cacheStats
)

var (
	// Cache provides access to an in-memory cache with per-item locking.
	Cache cache
)

// ContentSize is a wrapper that allows size of complex content to be explcitly specified rather
// than computed. Both cache item and cache group Set methods will accept content in this form.
// To determine which content types require explicit size provision, see the ByteCount function.
// Set methods rely on this function to calculate size, so if your type is not covered there,
// then you will need to provide the size using this wrapper.
type ContentSize struct {
	content interface{}
	size    int
}

// DurationMap maps cache durations to cache keys
type DurationMap map[string]time.Duration

// byHits implements sort.Interface our hit list for pruning purposes
type byHitCount []cacheHits

func (a byHitCount) Len() int           { return len(a) }
func (a byHitCount) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byHitCount) Less(i, j int) bool { return a[i].count < a[j].count }

type cache struct {
	cleanMinInterval     uint16
	cleanReset           chan bool
	command              chan cacheCommand
	duration             map[string]int64
	euthanizeAfter       float32
	euthanizeMinInterval uint16
	groups               map[string]*cacheGroup
	infoSize             int
	items                map[string]*cacheItem
	keyError             string
	prune                chan interface{}
	pruneAt              int64
	pruneTo              int64
	pruneMinInterval     uint16
	registered           bool
	size                 chan int64
	stats                chan cacheStatsSummary
	setMinInterval       uint8
}

// Clean cycles through members of the cache to remove any stale content. The interval from
// one cleaning to the next is controlled by setting Conf.Cache.CleanMinInterval
func (c *cache) Clean() {
	Cache.command <- cacheCommand{do: cacheClean}
}

// Clear deletes all items from the cache
func (c *cache) Clear() {
	size := 0
	for key, entry := range c.items {
		entry.Lock()
		size += entry.item.size
		entry.item = cacheItemInfo{}
		entry.Unlock()

		// Remove associated hit data
		Cache.command <- cacheCommand{do: cacheHitDelete, key: key}
	}

	// Adjust cache size
	Cache.size <- <-Cache.size - int64(size)

	for _, group := range c.groups {
		group.Clear()
	}
}

// Content is a convenience method that returns any unexpired content currently in the cache for
// the given key, if any exists. If it does not exist or is expired, Content uses the provided
// content and duration to set the content for this key before returning it. Like the Set method,
// content may either be a value or a function of the form func() interface{}. In the latter case,
// content will be derived from the return value of this function. Note that this method should
// only be used for content that is either immutable, such as strings and numbers, or has its own
// mutex.
func (c *cache) Content(key string, content interface{}, duration ...time.Duration) interface{} {
	if current := c.Get(key); current != nil {
		return current
	}

	c.Set(key, content, duration...)
	return c.Get(key)
}

// CallbackContent is identical to Content, except that it uses a callback to extract relevant
// data from a cache value, while the item is read locked, and returns the return value of that
// callback instead of the cache value itself. In this manner, data stored in slices, maps, and
// structs can be safely retrieved without the need for the additional mutexes that would be
// required if the value itself were returned. See the description for the Get method for more.
func (c *cache) CallbackContent(key string, callback func(content interface{}) interface{}, content interface{}, duration ...time.Duration) interface{} {
	if current := c.Get(key, callback); current != nil {
		return current
	}

	c.Set(key, content, duration...)
	return c.Get(key, callback)
}

// Delete removes items with the specified keys from the cache. If no such key exists in the cache,
// nothing will happen. To delete items from a group, use the Group method to access the group in
// question.
func (c *cache) Delete(keys ...string) {
	size := 0
	for _, key := range keys {
		// Remove provided keys from this group map
		if entry, ok := c.items[key]; ok {
			entry.Lock()
			size += entry.item.size
			entry.item = cacheItemInfo{}
			entry.Unlock()

			// Remove associated hit data
			Cache.command <- cacheCommand{do: cacheHitDelete, key: key}
		} else {
			Panicf(c.keyError, key)
		}
	}

	// Adjust cache size
	Cache.size <- <-Cache.size - int64(size)
}

// Euthanize removes members of the cache that have lived more than a certain percent of their lives.
// By default, this is specified in Conf.Cache.EuthanizeAfter but can also be provided as an argument
// if more or less is desired. Whenever pruning occurs, euthanization is always the first step in this
// process.
func (c *cache) Euthanize(euthanizeAfter ...uint8) {
	if len(euthanizeAfter) > 0 {
		// Negative indicates euthanize only, magnitude indicates percentage to keep
		c.prune <- -float32(100-euthanizeAfter[0]) / 100
	} else {
		// Euthanize only, using Conf.Cache.EuthanizeAfter for cutoff
		c.prune <- false
	}
}

// Get accepts a key for its first argument and optionally a callback for its second. If the
// key refers to expired content, it returns nil. If unexpired content is available for this
// key and no callback is provided, it returns the corresponding value. If a callback is
// provided, it will receive the value in the cache as an argument and will be expected to
// return whatever value needs to be returned from the call to Get.
//
// Be aware that only values that are copied when returned, such as strings and numbers, are
// thread safe. Slices, maps, and structs are not. If you need to access one of these types
// of objects in the cache, you will either need to embed them in a struct with its own mutex
// or provide a callback that can be used to extract and manipulate the relevant portions of
// the object, in which case the item will remain read locked until the callback completes.
// Of course, any embedded objects would also need to be locked or copied before being return
// to prevent errors under racy conditions.
//
// When using a callback to work with mutable objects, whatever happens within the callback
// will be executed within a read lock for that item, so that data can safely be extracted
// and put into a format that will be safe to return to the calling request. Whatever value
// is returned from the callback will in turn be returned by Get. Although it may be possible
// to alter an object within the callback, this will produce errors under racy conditions, so
// be careful only to read data within the callback and use Set to make any alterations.
func (c *cache) Get(key string, callback ...func(content interface{}) interface{}) interface{} {
	hit := false
	if entry, ok := c.items[key]; ok {
		entry.RLock()
		defer func() {
			entry.RUnlock()
			if hit {
				// Register a hit
				Cache.command <- cacheCommand{do: cacheHit, key: key}
			}
		}()

		if item := entry.item; item.size != 0 && item.expires > time.Now().Unix() {
			hit = true
			if len(callback) > 0 {
				// Return the result of the callback
				return callback[0](item.content)
			}

			// Return the item as is
			return item.content
		}
	} else {
		Panicf(c.keyError, key)
	}

	return nil
}

// Group provides access to the group with the given key. Members of that group may be manipulated
// using the returned object's methods.
func (c *cache) Group(key string) *cacheGroup {
	group, ok := c.groups[key]
	if ok {
		return group
	}
	Panicf("No group with key \"%s\" is registered; use RegisterCache to create it", key)
	return &cacheGroup{}
}

// Prune initiates pruning of the cache. This will prune the cache to Conf.Cache.PruneTo percent of
// the max cache size. Before pruning, any cache items that have lived Conf.Cache.EuthanizeAfter
// percent of their lives will be removed. This percent may be altered for a given call by providing
// euthanizeAfter in this method's first argument. Note, that pruning automatically occurs whenever
// the maximum cache size is exceeded, so manual calls should seldom be necessary.
func (c *cache) Prune(euthanizeAfter ...uint8) {
	if len(euthanizeAfter) > 0 {
		// Positive indicates euthanization and pruning, magnitude indicates percentage to keep
		c.prune <- float32(100-euthanizeAfter[0]) / 100
	} else {
		// Euthanize then prune, using Conf.Cache.EuthanizeAfter for cutoff
		c.prune <- true
	}
}

// Set is used to populate cache content. It takes a unique key by which to identify content,
// the content to assign, and optionally the duration in seconds that content should remain
// in the cache. If no duration is provided, the duration provided in Cache.Register() will be
// used. Set returns the cached content when done. As with the Get method, returned slices,
// maps, and structs are not thread safe, so be careful to lock on these before use or use
// a callback with Get to extract what you need.
func (c *cache) Set(key string, content interface{}, duration ...time.Duration) {
	entry, ok := c.items[key]
	if !ok {
		Panicf(c.keyError, key)
	}

	size, newSize, seconds, expires, now, item := int64(0), 0, int64(0), int64(0), time.Now().Unix(), entry.item
	entry.Lock()
	defer func() {
		entry.Unlock()
		if seconds > 0 {
			// Indicates a Set happened
			c.command <- cacheCommand{cacheHitSet, key, &cacheHits{0, seconds, expires, "", key}}

			// Adjust cache size and make sure it's within bounds
			size = <-Cache.size + size + int64(newSize)
			Cache.size <- size
			if size > Conf.Cache.MaxSize {
				// Succeeds or blocks until pruning is complete
				Cache.prune <- true
			}
		}
	}()

	if item.size != 0 {
		if now < item.expires-item.duration+int64(Cache.setMinInterval) {
			// Item was set less than Conf.Cache.SetMinInterval seconds ago; keep current content
			return
		}

		// Record the current item's size
		size = int64(-item.size)
	}

	// Get new content, size, and seconds
	content, newSize = c.getContentSize(content)
	if len(duration) > 0 {
		seconds = int64(duration[0].Seconds())
	} else {
		seconds = Cache.duration[key]
	}

	// Create the new member, using current time (now may be out of date depending on content generated)
	now = time.Now().Unix()
	expires = now + seconds
	entry.item = cacheItemInfo{content, seconds, expires, newSize}
}

// Size returns the current size of the cache
func (c *cache) Size() int64 {
	size := <-Cache.size
	Cache.size <- size
	return size
}

// Stats locks the cache and takes a snapshot of the cache at it stands at a given point
// in time. It then returns several helpful numbers for each item in the cache, for each
// group, and for the overall cache. This data can be processed for whatever view you like
// or you can use StatsReport to generate a readable report.
func (c *cache) Stats() cacheStatsSummary {
	c.command <- cacheCommand{do: cacheStats}
	return <-c.stats
}

// StatsReport generates a readable report for representing various statistics returned
// by the Stats method. It uses cache/stats.html in the templates folder to for display,
// but this template can be overridden with a template having the same path in your
// project's template directory if the default is not satisfactory. Note that the report
// generated by this method is minimally styled, so you should inspect the structure and
// style according to your needs.
func (c *cache) StatsReport() string {
	return FillTemplate("/cache/stats.html", c.Stats())
}

// getContent gets content from a callback or passes through the content argument as is.
func (c *cache) getContent(content interface{}) interface{} {
	if contentFunc, ok := content.(func() interface{}); ok {
		content = contentFunc()
	}

	return content
}

// getContentSize takes content or a ContentSize object. In the former case, it will
// attempt to calculate size by the nature of the content and will panic if it is not
// able to do so. For content types that would cause such a panic, content and size
// must be provided separately in a ContentSize wrapper. In either case, if content
// takes the form func() interface{}, it will be updated with the return value of this
// function and returned with the size.
func (c *cache) getContentSize(content interface{}) (interface{}, int) {
	if cs, ok := content.(ContentSize); ok {
		// Size must be provided for complex types
		return c.getContent(cs.content), cs.size
	}

	content = c.getContent(content)
	size := ByteCount(content)
	if size < 0 {
		Panicf("Could not calculate the size of the following content:\n%v", content)
	}

	return content, size
}

// runProcesses starts up the cleaning and command processes for the cache
func (c *cache) runProcesses() {
	// Start the cleaning timer process
	go func() {
		for {
			select {
			case <-c.cleanReset:
			case <-time.After(Conf.Cache.CleanInterval):
				Cache.command <- cacheCommand{do: cacheClean, key: "!"}
			}
		}
	}()

	// Start the cache command process
	go func() {
		hits, lastCleaning, lastEuthanization := make(map[string]*cacheHits), int64(0), int64(0)

		for {
			select {
			case c := <-Cache.command:
				switch c.do {
				case cacheHit:
					if counter, ok := hits[c.key]; ok {
						counter.count += 1
					}
				case cacheHitDelete:
					if c.key != "" {
						// Single key delete
						delete(hits, c.key)
					} else {
						// Multiple item delete
						for _, key := range c.x.([]string) {
							delete(hits, key)
						}
					}
				case cacheHitSet:
					hits[c.key] = c.x.(*cacheHits)
				case cacheClean:
					if c.key != "!" {
						// Manually triggered cleaning; reset the timer
						Cache.cleanReset <- true
					}

					now := time.Now().Unix()
					if now-lastCleaning >= int64(Cache.cleanMinInterval) {
						// It's been the minimum interval between cleanings; okay to clean again
						size := int64(0)
						for key, entry := range Cache.items {
							entry.Lock()
							if item := entry.item; item.size != 0 && item.expires < now {
								// Item exists and is expired
								size += int64(item.size)
								entry.item = cacheItemInfo{}
								delete(hits, key)
							}
							entry.Unlock()
						}

						// Now the same for group items
						for name, group := range Cache.groups {
							groupSize := int64(0)
							group.Lock()
							for key, item := range group.items {
								if item.expires < now {
									groupSize += int64(item.size)
									delete(group.items, key)
									delete(hits, key+":"+name)
								}
							}

							// Adjust group size
							group.size -= groupSize
							group.Unlock()

							// Add this to size adjustment for overall cache
							size += groupSize
						}

						// Adjust cache size for removed items
						size = <-Cache.size - size
						Cache.size <- size

						lastCleaning = now
					}
				case cacheStats:
					// Lock all items and group items and capture item data
					itemsSize, groups, i, itemCount := int64(0), make([]string, len(Cache.groups)), 0, 0
					for _, entry := range Cache.items {
						entry.Lock()
						itemsSize += int64(entry.item.size)
					}
					for key, group := range Cache.groups {
						group.Lock()
						groups[i] = key
						itemCount += len(group.items)
						i++
					}

					// Note the size AFTER everything is locked
					commands, precision, size := []cacheCommand{}, int(Conf.Cache.StatsPrecision), <-Cache.size
					Cache.size <- size

					// Read all remaining commands
					for len(Cache.command) > 0 {
						c := <-Cache.command
						switch c.do {
						case cacheHit:
							if counter, ok := hits[c.key]; ok {
								counter.count += 1
							}
						case cacheHitDelete:
							if c.key != "" {
								// Single key delete
								delete(hits, c.key)
							} else {
								// Multiple item delete
								for _, key := range c.x.([]string) {
									delete(hits, key)
								}
							}
						case cacheHitSet:
							hits[c.key] = c.x.(*cacheHits)
						default:
							// Preserve any other commands
							commands = append(commands, c)
						}
					}

					// The hit map is up to date and items locked; ready for our snapshot
					overall := cacheStatsOverall{
						GroupCount:     len(Cache.groups),
						GroupItemCount: itemCount,
						ItemCount:      itemCount + len(Cache.items),
						PrimaryCount:   len(Cache.items),
						Free:           Conf.Cache.MaxSize - size,
						FreePercent:    Round(float64((Conf.Cache.MaxSize-size)*100)/float64(Conf.Cache.MaxSize), precision),
						Size:           Conf.Cache.MaxSize,
						Used:           size,
						UsedPercent:    Round(float64(size*100)/float64(Conf.Cache.MaxSize), precision),
					}

					now, totalHits, groupHits, totalPop, groupPop, i := time.Now().Unix(), 0, 0, 0, 0, 0
					items := make([]cacheStatsItem, len(Cache.items))
					sections := make([]cacheStatsSection, len(Cache.groups)+1)

					// Loop through primary cache items and perform calculations
					for key, entry := range Cache.items {
						item, hit, pctLeft := entry.item, hits[key], float64(0)
						if hit.expires > now {
							pctLeft = float64(hit.expires-now) / float64(hit.duration)
						}
						itemPop := int(float64(hit.count) * pctLeft)
						groupHits += hit.count
						groupPop += itemPop

						items[i] = cacheStatsItem{
							Duration:         time.Duration(item.duration) * time.Second,
							Expires:          time.Unix(item.expires, 0),
							Hits:             hit.count,
							Key:              key,
							Popularity:       itemPop,
							Remaining:        time.Duration(item.expires-now) * time.Second,
							RemainingPercent: Round(pctLeft*100, precision),
							Size:             item.size,
						}
						i++
						entry.Unlock()
					}
					totalHits += groupHits
					totalPop += groupPop

					// Create the primary item section and assign our items
					sections[0] = cacheStatsSection{
						cacheStatsGroup{
							Hits:       groupHits,
							Name:       "Primary",
							Popularity: groupPop,
							Size:       itemsSize,
						},
						items,
					}

					// Loop through group cache items and perform calculations
					sort.Strings(groups)
					for j, name := range groups {
						group := Cache.groups[name]
						groupHits, groupPop, i, items = 0, 0, 0, make([]cacheStatsItem, len(group.items))
						for key, item := range group.items {
							hit, pctLeft := hits[key+":"+name], float64(0)
							if hit.expires > now {
								pctLeft = float64(hit.expires-now) / float64(hit.duration)
							}
							itemPop := int(float64(hit.count) * pctLeft)
							groupHits += hit.count
							groupPop += itemPop

							items[i] = cacheStatsItem{
								Duration:         time.Duration(item.duration) * time.Second,
								Expires:          time.Unix(item.expires, 0),
								Hits:             hit.count,
								Key:              key,
								Popularity:       itemPop,
								Remaining:        time.Duration(item.expires-now) * time.Second,
								RemainingPercent: Round(pctLeft*100, precision),
								Size:             item.size,
							}
							i++
						}
						totalHits += groupHits
						totalPop += groupPop

						sections[j+1] = cacheStatsSection{
							cacheStatsGroup{
								Name:       strings.Title(name),
								Hits:       groupHits,
								Popularity: groupPop,
								Size:       group.size,
							},
							items,
						}
						group.Unlock()
					}

					// Now we have total hits and popularity points; calculate group and item percentages
					for sk, section := range sections {
						g := &section.Group
						sections[sk].Group.Percent = cacheStatsPercent{
							Hits:       Round(float64(g.Hits*100)/float64(totalHits), precision),
							Popularity: Round(float64(g.Popularity*100)/float64(totalPop), precision),
							Size:       Round(float64(g.Size*100)/float64(size), precision),
						}
						for ik, item := range section.Items {
							section.Items[ik].Percent = cacheStatsPercent{
								Hits:            Round(float64(item.Hits*100)/float64(totalHits), precision),
								GroupHits:       Round(float64(item.Hits*100)/float64(g.Hits), precision),
								Popularity:      Round(float64(item.Popularity*100)/float64(totalPop), precision),
								GroupPopularity: Round(float64(item.Popularity*100)/float64(g.Popularity), precision),
								Size:            Round(float64(int64(item.Size)*100)/float64(size), precision),
								GroupSize:       Round(float64(int64(item.Size)*100)/float64(g.Size), precision),
							}
						}
					}

					// Return the stats summary, unblocking the caller
					Cache.stats <- cacheStatsSummary{sections, overall}

					// Reinsert remaining commands into queue
					go func() {
						for _, command := range commands {
							Cache.command <- command
						}
					}()
				}

			case p := <-Cache.prune:
				removed, now, after, prune := int64(0), time.Now().Unix(), Cache.euthanizeAfter, true

				// Manual calls allow for euthanization without pruning and custom cutoffs
				switch t := p.(type) {
				case float32:
					if t < 0 {
						t *= -1
						prune = false
					}
					after = t
				case bool:
					prune = t
				}

				// Euthanizing too often is not helpful; make sure the minimum interval has passed
				if now-lastEuthanization > int64(Cache.euthanizeMinInterval) {
					// Start by removing any items that are expired or soon to expire
					for key, entry := range Cache.items {
						entry.Lock()
						if item := entry.item; item.size != 0 && item.expires-now < int64(float32(item.duration)*after) {
							removed += int64(item.size)
							entry.item = cacheItemInfo{}
							delete(hits, key)
						}
						entry.Unlock()
					}

					// Now the same for group items
					for name, group := range Cache.groups {
						groupSize := int64(0)
						group.Lock()

						for key, item := range group.items {
							if item.expires-now < int64(float32(item.duration)*after) {
								groupSize += int64(item.size)
								delete(group.items, key)
								delete(hits, key+":"+name)
							}
						}

						// Adjust group size
						group.size -= groupSize
						group.Unlock()

						// Add this to size adjustment for overall cache
						removed += groupSize
					}

					lastEuthanization = now
				}

				// Adjust the size
				size := <-Cache.size - removed
				Cache.size <- size

				// Only prune if (1) this is not a call from Euthanize and (2) size
				// after euthanization is still above our pruning threshhold
				if prune && size > Cache.pruneTo {
					// Process commands currently in the command channel to get the hits map up to date
					for len(Cache.command) > 0 {
						c := <-Cache.command
						switch c.do {
						case cacheHit:
							if counter, ok := hits[c.key]; ok {
								counter.count += 1
							}
						case cacheHitDelete:
							if c.key != "" {
								// Single key delete
								delete(hits, c.key)
							} else {
								// Multiple item delete
								for _, key := range c.x.([]string) {
									delete(hits, key)
								}
							}
						case cacheHitSet:
							hits[c.key] = c.x.(*cacheHits)
						}
						// Discard any other commands
					}

					// At this point, we may still have some commands in the queue, meaning that some items
					// in the hits map may have been deleted or updated since pruning was initiated, but these
					// changes will not be apparent in the map. Such items will be ignored for the purpose of
					// pruning. To be eligible for pruning, an item must (1) exist in the cache and (2) have
					// the same expiration as that recorded in the hit map at the time of evaluation.
					//
					// This design enables continued manipulation of the cache even while pruning is taking
					// place. Because a Set is possible during pruning, it may increase cache size beyond the
					// maximum size temporarily. However, any Set that would cause such an overage will try to
					// initiate a pruning and will either succeed or block until pruning is complete. The item
					// will be stored, but the request will be on pause until size is back within acceptable
					// limits.
					//
					// In short, Conf.Cache.MaxSize is not a hard and fast limit on the size of the cache.
					// Rather, it is the ideal maximum for the cache and the bound that once surpassed will
					// result in pruning.
					current, i, j, now := size, 0, 0, time.Now().Unix()
					hitList, newerHits := make([]cacheHits, len(hits)), make([]cacheHits, len(hits))
					for _, hit := range hits {
						newHit := cacheHits{
							group:   hit.group,
							key:     hit.key,
							expires: hit.expires,

							// Adjust the count for remaining life to determine popularity. This
							// normalizes hits, so newer items with fewer hits can be properly
							// compared to older items with a lot more hits.
							//
							// Popularity = HitCount * LifeRemaining / TotalLife
							count: int(int64(hit.count) * (hit.expires - now) / hit.duration),
						}

						if (now-(hit.expires-hit.duration))*100/hit.duration > int64(Conf.Cache.PruneAllowAfter) {
							// Older items will be considered for pruning first, regardless of popularity
							hitList[i] = newHit
							i++
						} else {
							// Newer items will be considered after older hits, giving them a chance to get hits
							newerHits[j] = newHit
							j++
						}
					}

					hitList = hitList[:i]
					newerHits = newerHits[:j]

					// Sort each of these from fewest to most hits then combine them
					sort.Sort(byHitCount(hitList))
					sort.Sort(byHitCount(newerHits))
					hitList = append(hitList, newerHits...)

					// The hit list will likely alternate groups; lock groups now to prevent
					// individual locking and unlocking for each item requiring removal.
					for _, group := range Cache.groups {
						group.Lock()
					}

					for _, hit := range hitList {
						if hit.group == "" {
							entry := Cache.items[hit.key]
							entry.Lock()
							if item := entry.item; item.size != 0 && item.expires == hit.expires {
								// Item has not been deleted or reset during the pruning process
								current -= int64(item.size)
								entry.item = cacheItemInfo{}
								delete(hits, hit.key)
							}
							entry.Unlock()
						} else {
							// Groups are all locked
							group := Cache.groups[hit.group]
							if item, ok := group.items[hit.key]; ok && item.expires == hit.expires {
								// Item has not been deleted or reset during the pruning process
								current -= int64(item.size)
								group.size -= int64(item.size)
								delete(group.items, hit.key)
								delete(hits, hit.key+":"+hit.group)
							}
						}

						if current < Cache.pruneTo {
							// We just dropped under our threshhold, so we're done pruning for now.
							// Be aware that we are under the threshhold based upon the cache size
							// when "over" was calculated. New items may have been added since our
							// pruning process began that would push us over the threshhold. Items
							// may also have been deleted, bringing us further under it.
							break
						}
					}

					for _, group := range Cache.groups {
						group.Unlock()
					}

					size = <-Cache.size - (size - current)
					Cache.size <- size
				}

				// If we're under the max size, it's safe to empty the prune channel and continue.
				// If not, it means we have had enough Sets happen during the pruning process to
				// put us back over, so we need to prune again via our loop-select construct.
				if size < Conf.Cache.MaxSize {
				L:

					for {
						select {
						case <-Cache.prune:
						default:
							break L
						}
					}
				}
			}
		}
	}()
}

type cacheCommand struct {
	do  int
	key string
	x   interface{}
}

type cacheGroup struct {
	sync.RWMutex
	items cacheGroupMap
	name  string
	size  int64
}

// Clean cycles through members of this group to remove any stale content. The group clean
// method is automatically called whenever an overall cache cleaning is initiated.
func (g *cacheGroup) Clean() {
	g.Lock()
	remove, size, now := []string{}, int64(0), time.Now().Unix()
	for key, item := range g.items {
		if item.expires < now {
			// Item is expired; remove it, noting size
			size += int64(item.size)
			delete(g.items, key)
			remove = append(remove, key+":"+g.name)
		}
	}
	g.size -= size
	g.Unlock()

	// Remove associated hit data and adjust cache size
	Cache.command <- cacheCommand{do: cacheHitDelete, x: remove}
	Cache.size <- <-Cache.size - size
}

// Clear deletes all items from this group
func (g *cacheGroup) Clear() {
	g.Lock()
	// Overwrite the group map and remove associated items from the hit map
	remove, size, i := make([]string, len(g.items)), g.size, 0
	for key := range g.items {
		remove[i] = key + ":" + g.name
		i++
	}
	g.items = make(cacheGroupMap)
	g.size = 0
	g.Unlock()

	// Remove associated hit data and adjust cache size
	Cache.command <- cacheCommand{do: cacheHitDelete, x: remove}
	Cache.size <- <-Cache.size - size
}

// Content is a convenience method that returns any unexpired content currently in this group for
// the given key, if any exists. If it does not exist or is expired, Content uses the provided
// content and duration to set the content for this key before returning it. Like the Set method,
// content may either be a value or a function of the form func() interface{}. In the latter case,
// content will be derived from the return value of this function. Note that this method should
// only be used for content that is either immutable, such as strings and numbers, or has its own
// mutex.
func (g *cacheGroup) Content(key string, content interface{}, duration ...time.Duration) interface{} {
	if current := g.Get(key); current != nil {
		return current
	}

	g.Set(key, content, duration...)
	return g.Get(key)
}

// CallbackContent is identical to Content, except that it uses a callback to extract relevant
// data from a cache value, while the item is read locked, and returns the return value of that
// callback instead of the cache value itself. In this manner, data stored in slices, maps, and
// structs can be safely retrieved without the need for the additional mutexes that would be
// required if the value itself were returned. See the description for the Get method for more.
func (g *cacheGroup) CallbackContent(key string, callback func(content interface{}) interface{}, content interface{}, duration ...time.Duration) interface{} {
	if current := g.Get(key, callback); current != nil {
		return current
	}

	g.Set(key, content, duration...)
	return g.Get(key, callback)
}

// Delete removes items with the specified keys from this group. If no such key exists in the
// group, nothing will happen.
func (g *cacheGroup) Delete(keys ...string) {
	g.Lock()
	// Remove items with keys from this group map and hit map
	remove, size := []string{}, int64(0)
	for _, key := range keys {
		if item, ok := g.items[key]; ok {
			size += int64(item.size)
			delete(g.items, key)
			remove = append(remove, key+":"+g.name)
		}
	}
	g.size -= size
	g.Unlock()

	// Remove associated hit data and adjust cache size
	Cache.command <- cacheCommand{do: cacheHitDelete, x: remove}
	Cache.size <- <-Cache.size - int64(size)
}

// Euthanize removes items in this group that have lived more than a certain percent of their lives.
// By default, this is specified in Conf.Cache.EuthanizeAfter but can also be provided as an argument
// if more or less is desired. Whenever pruning occurs, euthanization is always the first step in this
// process.
func (g *cacheGroup) Euthanize(euthanizeAfter ...uint8) {
	after := Cache.euthanizeAfter
	if len(euthanizeAfter) > 0 {
		after = float32(100-euthanizeAfter[0]) / 100
	}

	g.Lock()
	remove, size, now := []string{}, int64(0), time.Now().Unix()
	for key, item := range g.items {
		if item.expires-now < int64(float32(item.duration)*after) {
			// Item is expired or lived enough of its life to treat it as expired; remove it, noting size
			size += int64(item.size)
			delete(g.items, key)
			remove = append(remove, key+":"+g.name)
		}
	}
	g.size -= size
	g.Unlock()

	// Remove associated hit data and adjust cache size
	Cache.command <- cacheCommand{do: cacheHitDelete, x: remove}
	Cache.size <- <-Cache.size - size
}

// Get accepts a key for its first argument and optionally a callback for its second. If the
// key refers to expired content, it returns nil. If unexpired content is available for this
// key and no callback is provided, it returns the corresponding value. If a callback is
// provided, it will receive the value in the cache as an argument and will be expected to
// return whatever value needs to be returned from the call to Get. See the cache Get method
// for more on the callback and when and how it should be used.
func (g *cacheGroup) Get(key string, callback ...func(content interface{}) interface{}) interface{} {
	hit := false
	g.RLock()
	defer func() {
		g.RUnlock()
		if hit {
			Cache.command <- cacheCommand{do: cacheHit, key: key + ":" + g.name}
		}
	}()

	if item, ok := g.items[key]; ok && item.expires > time.Now().Unix() {
		hit = true
		if len(callback) > 0 {
			// Return the result of the callback
			return callback[0](item.content)
		}

		// Return the item as is
		return item.content
	}

	return nil
}

// Set is used to populate group content. It takes a unique key by which to identify content,
// the content to assign, and optionally the duration in seconds that content should remain
// in the cache. If no duration is provided, the duration provided in Cache.Register() will be
// used. Set returns the cached content when done. As with the Get method, returned slices,
// maps, and structs are not thread safe, so be careful to lock on these before use or use
// a callback with Get to extract what you need.
func (g *cacheGroup) Set(key string, content interface{}, duration ...time.Duration) {
	size, newSize, seconds, expires, now := int64(0), 0, int64(0), int64(0), time.Now().Unix()

	g.Lock()
	defer func() {
		g.Unlock()
		if seconds > 0 {
			// Indicates a Set happened
			Cache.command <- cacheCommand{cacheHitSet, key + ":" + g.name, &cacheHits{0, seconds, expires, g.name, key}}

			// Adjust cache size
			size = <-Cache.size + size
			Cache.size <- size

			// Make sure cache size is within bounds
			if size > Conf.Cache.MaxSize {
				// Succeeds or blocks until pruning is complete
				Cache.prune <- true
			}
		}
	}()

	if item, ok := g.items[key]; ok {
		if now < item.expires-item.duration+int64(Cache.setMinInterval) {
			// Item was set less than Conf.Cache.SetMinInterval seconds ago; keep current content
			return
		}

		// Record the current item's size
		size = int64(-item.size)
	}

	// Get new content, size, and seconds
	content, newSize = Cache.getContentSize(content)
	if len(duration) > 0 {
		seconds = int64(duration[0].Seconds())
	} else {
		seconds = Cache.duration[g.name]
	}

	// Account for size of struct and key, in addition to content
	newSize += Cache.infoSize + len(key)

	// Create the new member, using current time (now may be out of date depending on content generated)
	now = time.Now().Unix()
	expires = now + seconds
	g.items[key] = &cacheItemInfo{content, seconds, expires, newSize}
	size += int64(newSize)
	g.size += size
}

// Size returns the the size of this cache group
func (g *cacheGroup) Size() int64 {
	g.RLock()
	defer g.RUnlock()
	return g.size
}

type cacheGroupMap map[string]*cacheItemInfo

type cacheHits struct {
	count             int
	duration, expires int64
	group, key        string
}

type cacheItem struct {
	sync.RWMutex
	item cacheItemInfo
}

type cacheItemMap map[string]cacheItem

type cacheItemInfo struct {
	content           interface{}
	duration, expires int64
	size              int
}

type cacheStatsGroup struct {
	Hits       int               // Total hits for items in this group
	Name       string            // Group name
	Percent    cacheStatsPercent // Collection of percentages for overall hits, popularity, and size
	Popularity int               // Total popularity points, based on life left and hits
	Size       int64             // Group size in bytes
}

type cacheStatsItem struct {
	Duration         time.Duration     // Duration of item
	Expires          time.Time         // Time of expiration
	Hits             int               // Hits for this item
	Key              string            // Unique key
	Percent          cacheStatsPercent // Collection of percentages for overall and group hits, popularity, and size
	Popularity       int               // Popularity points for this item, based on life left and hits
	Remaining        time.Duration     // Duration of life remaining
	RemainingPercent float64           // Percent of life remaining
	Size             int               // Size in bytes
}

type cacheStatsOverall struct {
	GroupCount     int     // Number of groups in the cache
	GroupItemCount int     // Number of items in all groups
	ItemCount      int     // Number of total items
	PrimaryCount   int     // Number of primary items in the caceh
	Free           int64   // Bytes before SizeMax
	FreePercent    float64 // Percent of cache that is free
	Size           int64   // Max size in bytes
	Used           int64   // Current size in bytes
	UsedPercent    float64 // Percent of cache that is in use
}

type cacheStatsPercent struct {
	Hits            float64 // Percent hits for this item in the cache
	Popularity      float64 // Percent popularity of this item in the cache
	Size            float64 // Percent of cache size
	GroupHits       float64 // Percent hits for this item in this group
	GroupPopularity float64 // Percent popularity of this item in this group
	GroupSize       float64 // Percent of group size
}

type cacheStatsSection struct {
	Group cacheStatsGroup  // Contains stats for this group of items
	Items []cacheStatsItem // Contains stats for each item in this group
}

type cacheStatsSummary struct {
	Sections []cacheStatsSection // Group sections
	Overall  cacheStatsOverall   // Overall summary
}

// Register records cache and group item default durations in the duration maps provided.
//
// The first cache map is for predefined static-key items. For example, if I have a "navBar"
// that I want to cache on all of my pages, this key should occur in the first cache map
// along with the default duration for that cache item. These cache items would subsequently
// be referenced directly by their predefined keys, i.e. "navbar".
//
// The second cache map is for arbitrarily keyed content. For example, if I want to cache
// aribtrary query data from my database, I might create a "query" cache group in the second
// map and provide a default duration that will apply to all members of this group. These
// group items would then subsequently be accessible using the Group method, followed
// by the method corresponding to the operation to perform on the group.
//
// Cache items and groups corresponding to the keys from each duration map are initialized on
// server start, each with its own mutex. Thus, locking occurs on individual items and groups
// rather than on the cache as a whole, so that a lengthy Set operation for a particular cache
// item or group will only block access to that item, allowing uninterrupted access to unrelated
// items and groups.
//
// Once duration maps have been processed, administration processes are begun, including a
// cache cleaning timer and a command process.
//
// The cleaning timer process performs a issues a clean command every Conf.Cache.CleanInterval
// and resets the timer for the next cleaning. If a cleaning is triggered manually, this will
// also reset the timer, so that cleaning may happen more often but will happen at least as
// often as the duration specified in the cache configuration.
//
// The command process receives and processes commands that deal with the cache as a whole.
// It is responsible for tracking hits on each cache item and cleaning, euthanizing, or pruning
// the cache as needed. Hits are initialized via Set calls, accumulated via Get calls, and
// removed by any call that results in the item being deleted. Cleaning, euthanizing, and
// pruning can all be run manually or allowed to run automatically -- cleaning by timer and
// euthanizing and pruning whenever a Set method results in the cache's size exceeding the
// byte size in Conf.Cache.MaxSize.
//
// For more information on cleaning, euthanizing, and pruning, see the associated methods.
func (r *register) Cache(cacheItems DurationMap, cacheGroups DurationMap) {
	if !r.cache {
		c := &Conf.Cache

		if c.CommandBuffer < 10 {
			Log.Fatal("Conf.Cache.CommandBuffer size must be at least 10")
		}

		if c.CleanMinInterval.Seconds() < 5 {
			Log.Fatal("Conf.Cache.CleanMinInterval must be at least 5 seconds")
		}

		if c.EuthanizeAfter > 100 {
			Log.Fatal("Conf.Cache.EuthanizeAfter must be between 0 and 100")
		}

		if c.MaxSize < 268435456 {
			Log.Fatal("Conf.Cache.MaxSize at least 250 megabytes (268435456 bytes)")
		}

		if c.PruneAllowAfter > 100 {
			Log.Fatal("Conf.Cache.PruneAllowAfter must be between 0 and 100")
		}

		if c.PruneMinInterval.Seconds() < 5 {
			Log.Fatal("Conf.Cache.PruneMinInterval must be at least 5 seconds")
		}

		if c.PruneAt > 100 || c.PruneAt < 80 {
			Log.Fatal("Conf.Cache.PruneTo must be between 80 and 100")
		}

		if c.PruneTo >= c.PruneAt || c.PruneTo < 40 {
			Log.Fatal("Conf.Cache.PruneTo must be at least 40 and less than %s", c.PruneAt)
		}

		mutexSize := 24
		baseItemInfoSize := 16 + strconv.IntSize/8
		baseItemSize, baseGroupSize := mutexSize+baseItemInfoSize, mutexSize+8

		Cache = cache{
			cleanMinInterval:     uint16(c.CleanMinInterval.Seconds()),
			cleanReset:           make(chan bool),
			command:              make(chan cacheCommand, c.CommandBuffer),
			duration:             make(map[string]int64),
			euthanizeAfter:       float32(100-c.EuthanizeAfter) / 100,
			euthanizeMinInterval: uint16(c.EuthanizeMinInterval.Seconds()),
			groups:               make(map[string]*cacheGroup),
			infoSize:             baseItemInfoSize,
			items:                make(map[string]*cacheItem),
			keyError:             "No cache item with key \"%s\" is registered; use Register to create it",
			prune:                make(chan interface{}),
			pruneAt:              int64(float64(c.MaxSize) * float64(c.PruneAt) / 100),
			pruneTo:              int64(float64(c.MaxSize) * float64(c.PruneTo) / 100),
			pruneMinInterval:     uint16(c.PruneMinInterval.Seconds()),
			registered:           false,
			size:                 make(chan int64, 1),
			stats:                make(chan cacheStatsSummary),
			setMinInterval:       uint8(c.SetMinInterval.Seconds()),
		}

		// Record the durations for these items and initialize the items map
		size := 0
		for key, duration := range cacheItems {
			Cache.duration[key] = int64(duration.Seconds())
			Cache.items[key] = &cacheItem{}

			// Includes bytes for RWMutex, item name, item duration, item expiration, and item size
			size += len(key) + baseItemSize
		}

		// Record the durations for these groups and initialize the groups map
		for key, duration := range cacheGroups {
			if _, ok := Cache.duration[key]; ok {
				Log.Fatal("The group key \"%s\" already refers to a cache item and cannot be reused")
			}
			Cache.duration[key] = int64(duration.Seconds())
			Cache.groups[key] = &cacheGroup{
				items: make(cacheGroupMap),
				name:  key,
			}

			// Includes bytes for RWMutex, group name, and group size
			size += len(key)*2 + baseGroupSize
		}
		fmt.Println("BASE CACHE SIZE: ", size)

		Cache.size <- int64(size)
		Cache.runProcesses()
		r.cache = true
	}
}
