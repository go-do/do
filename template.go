package do

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"regexp"
	"strings"
)

// LoadTemplate is an alternative to ParseFiles that searches for embedded templates,
// beginning with the template provided in the first argument, adds any files found to
// the template set using their respective paths as names, parses template content,
// and returns the template set for further modication or execution.
//
// References to embedded template files should always begin with a leading slash, which
// is used to distinguish these templates as files, rather than merely named templates.
// The given path is then used to access the template file and parse it. Since templates
// are named according to their paths, Go's built-in execution mechanism is sufficient
// to fill in the templates from here.
//
// Additional arguments may also be provided. The second argument may contain a function
// map for the templates. This will be merged with the map in Conf.TemplateFuncs. The
// next arguments may contain alternative delimiters to use in parsing. If absent, either
// the delimiters in Conf.TemplateDelims will be used or default delimiters.
//
// Before parsing the contents of each template, any duplicate template definitions
// are replaced with new definitions of the form NAME___#, where NAME is the original
// definition name. In effect, this gives the first definition for NAME priority over
// subsequent definitions, emulating block overriding. Subsequent definitions are still
// parsed, but are not used, with the exception of the occurences of a "next" tag.
//
// The "next" quasi-tag is not an official part of the Go templating language. If one
// template has a definition X that overrides another definition X in a subsequent
// template, using {{ next X }} allows one to access the overridden definition. When
// placed within the definition of X, this emulates inheritance, whereby the contents
// of the next definition of X can be placed within the current definition of X. This
// tag may, however, be placed anywhere on the page after the initial definition of X,
// say, within the definition of another template we wish to override, for example.
func LoadTemplate(relFilePath string, args ...interface{}) (*template.Template, error) {
	eString := "LoadTemplate failed: %v"
	content, _, e := ReadTemplateFile(relFilePath[1:])
	if e != nil {
		Log.Error(eString, e)
		return nil, e
	}

	// Initialize the template set, funcs, and delims
	t := template.New(relFilePath).Funcs(Conf.TemplateFuncs)
	delims := []string{"{{", "}}"}
	if len(args) > 0 {
		t.Funcs(args[0].(template.FuncMap))
		if len(args) == 3 {
			delims = []string{args[1].(string), args[2].(string)}
			t.Delims(delims[0], delims[1])
		}
	} else if len(Conf.TemplateDelims) != 0 {
		delims = []string{Conf.TemplateDelims[0], Conf.TemplateDelims[1]}
		t.Delims(delims[0], delims[1])
	}
	qDelims := []string{regexp.QuoteMeta(delims[0]), regexp.QuoteMeta(delims[1])}

	// Find template tags referencing file and define / next tags
	templatePattern := regexp.MustCompile(qDelims[0] + "\\s*template\\s*\\\"\\/(.*?)\\\"\\s*.*?" + qDelims[1])
	definePattern := regexp.MustCompile(qDelims[0] + "\\s*(define|next)\\s*\\\"(.*?)\\\"\\s*(.*?)" + qDelims[1])

	// Keep track of how many same-name definitions we have
	counter := make(map[string]int)

	// Modify same-name definitions and replace next tags with the associated template references
	defineReplace := func(match string) string {
		submatch := definePattern.FindStringSubmatch(match)
		if count, ok := counter[submatch[2]]; ok {
			if submatch[1] == "define" {
				// Already defined; redefine with the same name and a unique suffix to prevent a panic
				counter[submatch[2]]++
				return delims[0] + "define \"" + submatch[2] + "___" + string(count) + "\"" + delims[1]
			}

			// Next will refer to a template we will define next time we encounter this name
			return delims[0] + "template \"" + submatch[2] + "___" + string(count) + "\" " + submatch[3] + delims[1]
		} else {
			// Not defined; define it and return as is -- "next" fails without a prior definition
			counter[submatch[2]] = 0
			return match
		}
	}

	// Parse the base template after replacements
	_, e = t.Parse(definePattern.ReplaceAllStringFunc(content, defineReplace))
	if e != nil {
		Log.Error(eString, e)
		return nil, e
	}

	// Check for references to embedded template files
	matches := templatePattern.FindAllStringSubmatch(content, -1)
	for i := 0; i < len(matches); i++ {
		if relFilePath = matches[i][1]; t.Lookup("/"+relFilePath) == nil {
			// We haven't parsed this file yet
			content, _, e = ReadTemplateFile(relFilePath)
			if e != nil {
				Log.Error(eString, e)
				return nil, e
			}

			// Replace duplicate define tags and rewrite next tags to templates
			content = definePattern.ReplaceAllStringFunc(content, defineReplace)
			_, e = t.New("/" + relFilePath).Parse(content)
			if e != nil {
				Log.Error(eString, e)
				return nil, e
			}

			// Add any embedded template files
			matches = append(matches, templatePattern.FindAllStringSubmatch(content, -1)...)
		}
	}

	// Return the template for further modifications prior to execution
	return t, nil
}

// ReadTemplateFile attempts to read in a template provided in the relative path, using the paths
// specified in Conf.TemplatePaths as a base. If found, it will return the template contents,
// the full path of the found template, and a nil error. If not found, it will return an empty
// string for contents and path and an error detailing the full paths that were attempted.
func ReadTemplateFile(relTemplPath string) (string, string, error) {
	var (
		content []byte
		e       error
		path    string
	)

	// Attempt to find a template in the path
	for _, path = range Conf.TemplatePaths {
		content, e = ioutil.ReadFile(path + relTemplPath)
		if e == nil {
			// A file in our template paths was read successfully
			break
		}
	}

	if e != nil {
		return "", path + relTemplPath, fmt.Errorf("open \"%s\": no such file in Conf.TemplatePaths; attempted: %s",
			relTemplPath, strings.Join(Conf.TemplatePaths, relTemplPath+", ")+relTemplPath)
	}

	return string(content), path + relTemplPath, nil
}

// FillTemplate is a convenience function that simplifies retrieving the filled in content of
// a template. If successful, it will return the filled in template. If not, it will log the
// error generated and return an empty string. This design is intended to offer some resilience
// for errors in partial-page templates, so that the user still receives output for the page
// as a whole, even if a particular section is missing.
func FillTemplate(file string, context interface{}) string {
	bp := bufferPool.Get().(*bytes.Buffer)

	t, e := LoadTemplate(file)
	if e != nil {
		return ""
	}

	e = t.Execute(bp, context)
	if e != nil {
		Log.Error("FillTemplate failed with base template \"%s\": %v", file, e)
		return ""
	}

	output := bp.String()
	bp.Reset()
	bufferPool.Put(bp)

	return output
}

// RegisterContext assigns the map provided to to the internal an internal context map that
// will be applied to all future requests. Context must be set before the server is started.
func (r *register) Context(context Map) {
	if !r.cache {
		contextMap = context
		r.context = true
	}
}
