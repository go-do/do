package do

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"
)

type stackElement struct {
	Caller  string
	Content template.HTML
	Line    int
	Path    string
	Extra   string
}

// Error is the type of object that used by the default error display to show errors
// that occurred during execution while Conf.Debug was set to true.
type Error struct {
	Description interface{}    // Error description
	Line        int            // Line number of primary error in Path
	Lines       template.HTML  // The lines from the file where the error occurred
	Path        string         // Path to file of primary error
	Stack       []stackElement // Stack with code snippets
}

// ErrorLine accepts an Error object along with a stackline formatted string and contents.
// Based upon the ErrorPattern of the error passed, this function will match components of
// the stackline and use them to populate the various parts of the Error object, including
// Path, Line, and Stack.
func ErrorLine(newError *Error, pattern *regexp.Regexp, stackLine, contents string, isPrimary bool) bool {
	if errorParts := pattern.FindStringSubmatch(stackLine); len(errorParts) != 0 {
		// Looks like a stack error line pair
		path, pc, caller := errorParts[1], errorParts[3], errorParts[4]
		line, _ := strconv.Atoi(errorParts[2])
		padding, inSource := Conf.ErrLines[2], strings.HasPrefix(path, Conf.Path["source"])

		if isPrimary {
			newError.Path = path
			newError.Line = line
			padding = Conf.ErrLines[0]
		} else if inSource {
			padding = Conf.ErrLines[1]
		}

		if padding > 0 {
			// Get file contents
			if contents == "" {
				bytes, e := ioutil.ReadFile(path)
				Err(e)

				contents = string(bytes)
			}

			// Record and display relevant lines
			lines, firstLine, lastLine := strings.Split(contents, "\n"), line-padding-1, line+padding
			if firstLine < 0 {
				firstLine = 0
			}
			if lastLine > len(lines) {
				lastLine = len(lines)
			}

			for i := firstLine; i < lastLine; i++ {
				errStyle := ""
				if line == i+1 {
					errStyle = " class='error'"
				}
				lines[i] = "<tr" + errStyle + "><td>" + strconv.Itoa(i+1) + "</td><td>" + lines[i] + "</td></tr>"
			}

			content := template.HTML("<table class='source'>" + strings.Join(lines[firstLine:lastLine], "") + "</table>")
			newError.Stack = append(newError.Stack, stackElement{
				Content: content,
				Line:    line,
				Path:    path,
			})
			if isPrimary {
				newError.Lines = content
			}
		} else {
			newError.Stack = append(newError.Stack, stackElement{caller, "", line, path, pc})
		}

		return true
	}

	return false
}

// showError provides a neatly formatted browser-friendly error display
// when Conf.Debug is set to true
func showError(w http.ResponseWriter, b *bytes.Buffer, r request, err interface{}) {
	defer func() {
		e := recover()
		if e != nil {
			// Log error and serve a simple 500 error page
			Log.Error("Error page failure: %v", e)
			t, _ := template.New("500").Parse("<!DOCTYPE html><html><body><h1>500 - Internal Server Error</h1></body></html>")
			t.Execute(w, nil)
		}

		b.Reset()
		bufferPool.Put(b)
	}()

	// TODO If error is in template, display lines from the template file in the stack.
	// 	NOTE: this cannot be done until the issue with html template error reporting is fixed
	//		currently it reports the correct line but the wrong file. should be fixed next version

	b.Reset()
	newError, errorPattern, stack := &Error{
		Description: err,
	}, regexp.MustCompile("^\\s*(.*?):(\\d+)\\s*([^\\s]*)\\s*(.*)"), make([]byte, 10240)
	runtime.Stack(stack, false)

	t, e := LoadTemplate("/error.html")
	Err(e)

	stackLines, primary := strings.Split(string(stack), "\n"), 0
	for i := len(stackLines) - 3; i > 2; i-- {
		// Locate the first panic
		fmt.Println(i, stackLines[i])
		if strings.HasPrefix(stackLines[i], "runtime.panic") {
			if strings.HasPrefix(stackLines[i+2], "bitbucket.org/go-do/do.Err") {
				primary = i + 5
			} else {
				primary = i + 3
			}
			break
		}
	}
	fmt.Println(primary)

	for i := len(stackLines) - 2; i > 2; i-- {
		// Match error lines matching the provided ErrorPattern
		isPrimary := primary == i
		if ErrorLine(newError, errorPattern, stackLines[i]+" "+stackLines[i-1], "", isPrimary) {
			i--
		}
	}

	cookieMap := make(Map)
	for _, v := range r.Cookies() {
		parts := strings.Split(fmt.Sprint(v), "=")
		cookieMap[parts[0]] = parts[1]
	}

	Err(t.Execute(b, Map{
		"Error":     newError,
		"Request":   &r,
		"GoVersion": strings.Replace(runtime.Version(), "go", "", 1),
		"Time":      time.Now().Local(),
		"Cookies":   cookieMap,
		"Conf":      Conf,
	}))
	b.WriteTo(w)

	Log.Debug(newError)
}

// Err tests for the presence of an error. If an error is found, it panics.
// If the error requires special formatting, you may pass the arguments for the
// fmt.Errof function following the error. These will be used to generate a
// custom error message in the event of an error.
func Err(e error, msg ...interface{}) {
	if e != nil {
		if len(msg) > 0 {
			e = fmt.Errorf(msg[0].(string), msg[1:]...)
		}
		panic(e)
	}
}

// Panicf is a convenience function that creates an error using the same
// arguments as fmt.Errorf and then panics on that error.
func Panicf(s string, args ...interface{}) {
	panic(fmt.Errorf(s, args...))
}
