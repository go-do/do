package do

import (
	"fmt"
	"log"
	"regexp"
	"sort"
	"strings"
)

// route describes a route as provided in the routes file of the project directory.
type route struct {
	Name    string         // The unique name of this route
	Params  []string       // A list of dynamic parameters in this route
	Path    string         // The route path, formatted for use with Url
	Pattern *regexp.Regexp // The pattern used to match the current route
	View    func(View)     // The view function to use when this route is matched
}

// Url attempts to apply the provided params to the Path of this route. If number of
// params does not match the number of Params in the route, it will panic. Otherwise,
// it will return the url Path with the given params.
func (r route) Url(params ...interface{}) string {
	paramCount := len(r.Params)
	if paramCount == 0 {
		// Fixed path
		return r.Path
	}

	if len(params) != paramCount {
		Panicf("Route %s (\"%s\") requires the following %d parameters: %s; %d parameters were provided",
			r.Name, r.Path, paramCount, strings.Join(r.Params, ", "), len(params))
	}

	// Dynamic path
	return fmt.Sprintf(r.Path, params...)
}

// routes maintains a record of Routes and their order
type routes struct {
	Order []string
	Map   map[string]route
}

// routeMatch contains current Route information and a map of values that matched the route
type routeMatch struct {
	*route
	ParamMap *map[string]string
}

var (
	Routes routes
)

// Url attempts to retrive a url pattern stored by name in Routes. If the route does not
// exist, it will panic. If it does exist, it will call the Url receiver method for the
// given route with the given params.
func Url(name string, params ...interface{}) string {
	route, ok := Routes.Map[name]
	if !ok {
		sorted := Routes.Order
		sort.Strings(sorted)
		Panicf("There is no route named \"%s\"; route options are: \"%s\"",
			name, strings.Join(sorted, "\", \""))
	}

	return route.Url(params...)
}

type rawRoute struct {
	path     string
	view     func(View)
	paramMap SMap
}

// Route allows a route to be recorded and later registered via RegisterRoutes. A route is composed
// of up to four components: a path, a name, a view, and a parameter map. The string path is required
// as the first argument and may be followed by a name, separated from the path by whitespace. A view
// may follow the first argument, where a view is any function that accepts ViewData as its argument.
// This in turn may be followed by a map of regular expressions for route parameters.  Let's look at
// each part in detail.
//
// The path portion of a route may contain alphanumeric characters, underscores, hyphens, colons,
// and slashes. Slashes act as dividers and colons indicate dynamic url segments, which are
// represented by named parameters. Other characters are used to compose the remainder of the url.
// An asterisk may also be included at the end of a path to indicate a wildcard match on the rest of
// the url. All root paths should begin with a slash, whereas relative paths should not.
//
// The name serves as a unique identifier for a route. Names may be alphanumeric, may include
// underscores, and may begin with a colon. A name preceded by a colon is a relative name and will
// be used in combination with the computed name of the previously registerd route. Otherwise, a
// name is considered to be absolute. Routes with absolute names can be referenced directly by that
// name. Routes with relative names build upon a prior route's name. For example, given an absolute
// route ABS with a sub-route named SUB1, which has its own sub-route SUB2, we would reference the
// SUB2 sub-route using "ABS:SUB1:SUB2". If a route needn't be referenced, the name may be omitted
// and the path will be used as a unique name instead, but helpful names are always recommended.
//
// Indentation of a line in the route file is important. No indentation indicates a top level path.
// One indent, indicates that this is a sub route of the prior route and can borrow from the prior
// route's path, name, and regex map in constructing its own route. More than one indent beyond the
// prior route is not allowed.  This design allows us to easily group together and prioritize paths
// into an easy-to-vizualize directory-like structure.
//
// Dynamic portions of the path may be indicated by preceding a regex word with a colon. These parameters
// will be extracted and saved in the order that they appear. They will be used to determine the number
// of arguments necessary when calculating a reverse route and to form a dictionary of the matched
// portions of the current url. The parameter values for the current request are made available in
// the view's request argument in request.Route.ParamMap.
//
// An example of some sequential routes passed to RegisterRoutes might look something like the
// following:
//
// 		"/media/:type/			media"
// 		"	:slug/					:detail"
// 		"		related/			:related"
// 		"		author/				:author"
// 		"	authors/				:authors"
//
// See that the whitespace prior to the path indicates relationship from one line to the next. The
// whitespace following the path, only separates the name from the path and can also be used to
// visually separate absolute and relative route names. The above lines would translate into the
// following route / reference combinations:
//
// 		"/media/:type/					media"
// 		"/media/:type/:slug/			media:detail"
// 		"/media/:type/:slug/related/	media:related"
// 		"/media/:type/:slug/author/		media:author"
// 		"/media/:type/authors/			media:authors"
//
// So say "media:detail" refers to the actual content of a media entry of some type. "media:related"
// might then yield a list of like media, and "media:author" might tell us more about the author of
// this article. Not the best design, but it demonstrates how we might structure our path and name
// combinations to yield something useful.
//
// The next argument is the view, which is the function that should be called when the prior path is
// matched. View functions are required for route lines that involve an action. If a route line does
// not have a view associated with it, then its various parts will be used only for constructing
// subsequent routes, and it will not be available in the final route map. Regardless of whether or
// not a view function is provided, each line's path and name will be built upon by its sub-routes,
// and its parameter map will be inherited and overwritten by its sub-routes.
//
// The final argument to this function is the parameter regular expression map. The default parameter
// pattern is "\w+". If you need to otherwise expand or limit the possible values for a particular
// parameter, then you will need to provide a parameter / regular expression map that contains patterns
// suitable for your purposes. For example, if you wanted the routes above to be limited to predefined
// "type" values, you might use the following:
//
// 		SMap({"type": "article|blog|news|video"})
//
// SMap is a convenience wrapper for maps with string values. With this in place, any paths that
// contain the "type" parameter will only be matched when one of these types appears in that
// parameter's slot.
func Route(parts ...interface{}) rawRoute {
	var (
		route    string
		view     func(View)
		paramMap SMap
	)

	l := len(parts)
	if l == 0 {
		Log.Fatal("An empty route was found in your route registrations")
	}

	route, ok := parts[0].(string)
	if !ok {
		Log.Fatal("Routes must minimally contain an initial string path on which to match urls; arguments provided were: %v", parts)
	}

	if l > 1 {
		view, ok = parts[1].(func(View))
		if ok {
			if l > 2 {
				paramMap, ok = parts[2].(SMap)
			}
		} else {
			paramMap, ok = parts[1].(SMap)
		}

	}

	return rawRoute{route, view, paramMap}
}

// RegisterRoutes accepts the objects created by the Route function as arguments. The details of how
// these routes should be constructed may be found under that function. An example call to this function
// might look like the following:
//
// 		do.RegisterRoutes(
// 			do.Route("/media/:type/			media", yourapp.MediaList, do.SMap({"type": "article|blog|news|video"})
// 			do.Route("	:slug/					:detail", yourapp.MediaDetail
// 			do.Route("		related/			:related", yourapp.RelatedMedia
// 			do.Route("		author/				:author", yourapp.AuthorDetail
// 			do.Route("	authors/				:authors", yourapp.AuthorList
// 		)
//
// Routes are created and registered in the order received, such that an sub-routes are able to build upon
// and inherit from parent routes, as indicated by leading indentation in each line.
func (r *register) Routes(lines ...rawRoute) {
	log.Print("Initializing router . . .")

	routeNum, errorPrefix := 0, "Route \"%s\" in line number %d of your routes "
	routePattern, paramPattern := regexp.MustCompile("^(\\s*)([:\\w/-]+)(\\*?)\\s+(:?\\w*)\\s*$"), regexp.MustCompile(":(\\w+)")
	pathsByLevel, namesByLevel, regexByLevel := make([]string, 1), make([]string, 1), make([]map[string]string, 1)

	// Build routes line by line
	Routes = routes{make([]string, len(lines)), make(map[string]route)}
	for lineNum, line := range lines {
		routeParts := routePattern.FindStringSubmatch(line.path)
		if len(routeParts) == 0 {
			Log.Fatal(errorPrefix+"did not match the allowable route pattern: %v",
				line, lineNum+1, routePattern)
		}

		// Heirarchy is based on indentation by leading whitespace
		level, currentLevel := len(routeParts[1]), len(namesByLevel)
		path, wild, name := routeParts[2], routeParts[3], routeParts[4]
		paramRegex := make(SMap)

		if level == currentLevel {
			// Go up one level
			pathsByLevel = append(pathsByLevel, path)
			namesByLevel = append(namesByLevel, "")
			regexByLevel = append(regexByLevel, paramRegex)
		} else if level > currentLevel {
			Log.Fatal(errorPrefix+"skips a drill down level; check that it has no more than one more leading white space that the previous route", line, lineNum+1)
		} else {
			// Insert at the appropriate level
			pathsByLevel[level] = path
		}

		// Derive path based on this and prior levels
		path = strings.Join(pathsByLevel[0:level+1], "")
		if wild == "*" {
			wild = ".*"
		}

		// Derive name
		if name != "" {
			// An absolute or relative name was given
			if strings.HasPrefix(name, ":") {
				if level > 0 {
					// Relative name builds on the previous level
					name = namesByLevel[level-1] + name
				} else {
					// Root level relative names are not allowed
					Log.Fatal(errorPrefix+"contains a relative name at the root level; choose an absolute name", line, lineNum+1)
				}
			}
		} else {
			// No name was given; use our path as a unique name
			name = path
		}
		if _, ok := Routes.Map[name]; ok {
			// We can't have any duplicates
			Log.Fatal("The route in line %d of your routes file has the same name as a prior route with path \"%s\"", lineNum+1, Routes.Map[name].Path)
		}
		namesByLevel[level] = name

		// Derive parameter regex for this level
		if level > 0 {
			// Copy previous level field regexes for use in current level
			for param, value := range regexByLevel[level-1] {
				paramRegex[param] = value
			}
		}

		if line.paramMap != nil {
			// Populate / overwrite previous level regexes with current level
			for param, value := range line.paramMap {
				paramRegex[param] = value
			}
		}
		regexByLevel[level] = paramRegex

		params := make([]string, 0)
		if paramMatches := paramPattern.FindAllStringSubmatch(path, -1); len(paramMatches) != 0 {
			for _, match := range paramMatches {
				params = append(params, match[1])
			}
		}

		regexPath := path
		for _, param := range params {
			if paramRegex[param] != "" {
				regexPath = strings.Replace(regexPath, ":"+param, "("+paramRegex[param]+")", 1)
			}
		}

		if line.view != nil {
			// Only add this to Routes if the view is non-blank
			Routes.Order[routeNum] = name
			Routes.Map[name] = route{name, params, paramPattern.ReplaceAllString(path, "%v"),
				regexp.MustCompile("^" + paramPattern.ReplaceAllString(regexPath, "(\\w+)") + wild + "$"), line.view}
			routeNum++
		}
	}

	// Remove empty routes
	Routes.Order = Routes.Order[:len(Routes.Map)]

	// Make a neat record of available routes in the log
	routeSummary := ""
	for _, routeName := range Routes.Order {
		route := Routes.Map[routeName]
		routeSummary += fmt.Sprintf("\t%s => %s\n\t\tPath: %s\n\t\tParameters: %v\n\t\tMatches: %v\n",
			routeName, FuncName(route.View), route.Path, route.Params, route.Pattern)
	}

	log.Print(". . . router initialized with the following routes\n\n" + routeSummary + "\n")
}
