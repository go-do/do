package do

import (
	"go/build"
	"html/template"
	"log"
	"path/filepath"
	"reflect"
	"strings"
	"time"
)

type (
	conf struct {
		Cache          CacheConf
		Debug          bool
		ErrLines       [3]int // Primary Error, In Source Folder, Not In Source Folder
		Log            LogConf
		Path           map[string]string
		TemplateDelims []string // []string{"[[", "]]"} for example
		TemplateFuncs  template.FuncMap
		TemplatePaths  []string
		Version        string
	}

	register struct {
		cache   bool
		context bool
		css     bool
		js      bool
		routes  bool
	}

	CacheConf struct {
		CommandBuffer        uint16
		CleanInterval        time.Duration // Clean cache this often
		CleanMinInterval     time.Duration // The minimum duration between cleanings
		EuthanizeAfter       uint8         // Percent of life lived after which it's okay to kill an unexpired item, if needed
		EuthanizeMinInterval time.Duration // The minimum duration between euthanizations
		MaxSize              int64         // Maximum cache size in bytes
		PruneAllowAfter      uint8         // Disallow pruning of items having lived less than this percent of their lives
		PruneAt              uint8         // Percent of CacheMaxSize to initiate pruning at
		PruneMinInterval     time.Duration // The minimum duration between prunings
		PruneTo              uint8         // Percent of CacheMaxSize to prune the cache to when it gets too big
		SetMinInterval       time.Duration // Disallow subsequent sets on key X until this time has passed
		StatsPrecision       uint8         // The number of decimal places to which to round stats percents
	}

	LogConf struct {
		IsDated    bool   // Specifies whether FileFormat is date based
		FileFormat string // String that determines location and form of log files
		Flags      int    // Standard log flags that determine what is shown in the log
		ShowStack  bool   // If true, shows the stack in the log for the Fatal, Error, and Debug loggers
	}
)

var (
	Conf = conf{
		Cache: CacheConf{
			CommandBuffer:        300,
			CleanInterval:        time.Hour * 12,
			CleanMinInterval:     time.Second * 30,
			EuthanizeMinInterval: time.Second * 30,
			EuthanizeAfter:       80,
			MaxSize:              4294967296,
			PruneAllowAfter:      1,
			PruneAt:              95,
			PruneMinInterval:     time.Second * 30,
			PruneTo:              70,
			SetMinInterval:       time.Second * 3,
			StatsPrecision:       3,
		},
		Debug:    true,
		ErrLines: [3]int{10, 5, 0},
		Log: LogConf{
			IsDated:    true,
			FileFormat: "log/2006-01/02.log",
			Flags:      log.Ltime,
			ShowStack:  true,
		},
		Path: map[string]string{"root": build.Default.GOPATH + "/"},
		TemplateFuncs: template.FuncMap{
			// Return joined parts on a separator
			"join": strings.Join,

			// Repeat a string X times
			"repeat": strings.Repeat,

			// Reverses a string or slice
			"reverse": Reverse,

			// GO HAS ITS OWN HTML FUNCTION, USE THAT INSTEAD
			// Return the given string as valid HTML
			// "html": func(s string) template.HTML {
			// 	return template.HTML(s)
			// },

			// Return the string name of the given type
			"type": func(i interface{}) string {
				return reflect.TypeOf(i).String()
			},

			"url": Url,
		},
		Version: "1.0.0",
	}
	Register register
)

func init() {
	project, e := filepath.Abs(".")
	Err(e)

	Conf.Path["source"] = Conf.Path["root"] + "src/"
	Conf.Path["project"] = project + "/"
	Conf.Path["do.project"] = Conf.Path["source"] + "bitbucket.org/go-do/do/"
	for _, folder := range []string{"static", "template", "upload", "view"} {
		Conf.Path["do."+folder] = Conf.Path["do.project"] + folder + "/"
		Conf.Path[folder] = Conf.Path["project"] + folder + "/"
	}
	Conf.Path["css"] = Conf.Path["static"] + "css/"
	Conf.Path["js"] = Conf.Path["static"] + "js/"

	Conf.TemplatePaths = []string{Conf.Path["template"]}
}

// TODO List
//
// Error page needs to be improved for templates once the next version of Go is out. Currently
// having issues with referencing the correct file where the error occurred, although it does
// report the proper line. Once this is fixed, need to be able to display the error line of the
// template file on the error page.
//
// Need a css builder that works along the lines of node but without all of the hassle. Would
// allow css to be built programmatically and then assembled in a compressed format for transport.
// Naturally, variables and functions would be possible here. Would also allow for inheritance
// of previously specified properties by location in heirarchy.
//
