package do

import (
	"bytes"
	"log"
	"os"
	"runtime"
	"strings"
	"time"
)

// logType is the type used to create Log
type logType struct {
	file       string   // String representation of fileHandle
	fileHandle *os.File // The most recent open log file
}

// Fatal uses prints an Error to Logger.Fatal; records a critical error
func (l *logType) Fatal(e interface{}, args ...interface{}) {
	l.Print(Logger.Fatal, e, args...)
	os.Exit(1)
}

// Error uses prints an Error to Logger.Error; records a recoverable error
func (l *logType) Error(e interface{}, args ...interface{}) {
	l.Print(Logger.Error, e, args...)
}

// Warning uses prints an Error to Logger.Warning; warns of conditions that may cause an error
func (l *logType) Warning(e interface{}, args ...interface{}) *logType {
	return l.Print(Logger.Warning, e, args...)
}

// Info uses prints an Error to Logger.Info; record info
func (l *logType) Info(e interface{}, args ...interface{}) *logType {
	return l.Print(Logger.Info, e, args...)
}

// Debug uses prints an Error to Logger.Debug; use for debugging
func (l *logType) Debug(e interface{}, args ...interface{}) *logType {
	return l.Print(Logger.Debug, e, args...)
}

// Init preps and opens the appropriate log file for the current date. Set Conf.LogIsDated to
// false if the log file is static. Set Conf.LogFlags to adjust logger flags. This method is
// executed automatically on server start and with every new request. If Conf.LogIsDated is set
// to true, tt should also be executed for any other date-sensitive scripts that make use of
// one or more Loggers.
func (l *logType) Init() *logType {
	loggerFile := Conf.Log.FileFormat
	if Conf.Log.IsDated {
		// The log file has a date component
		loggerFile = time.Now().Format(loggerFile)
		if Log.file == loggerFile {
			// Logger file represents the correct date already
			return l
		}
	} else if l.file != "" {
		// Static log, already initialized
		return l
	}

	// Create any directories needed for this path
	parts := strings.Split(loggerFile, "/")
	if len(parts) > 1 {
		logPath := strings.Join(parts[0:len(parts)-1], "/")
		if os.MkdirAll(logPath, 0777) != nil {
			log.Fatalf("Could not create log path directories: %s", logPath)
		}
	}

	// Assign the log file
	file, e := os.OpenFile(loggerFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if e != nil {
		log.Fatalf("Could not open log file: %s", loggerFile)
	}
	if Log.file != "" {
		// For dated logs, be sure to close the previous date's log file
		oldFile := Log.fileHandle
		defer oldFile.Close()
	}

	// Create persistent loggers
	Logger = &loggerType{
		log.New(file, "FATAL: ", Conf.Log.Flags),
		log.New(file, "ERROR: ", Conf.Log.Flags),
		log.New(file, "WARNING: ", Conf.Log.Flags),
		log.New(file, "INFO: ", Conf.Log.Flags),
		log.New(file, "DEBUG: ", Conf.Log.Flags),
	}

	// Update the main log
	log.SetOutput(file)
	log.SetPrefix("LOG: ")
	log.SetFlags(Conf.Log.Flags)

	Log.file = loggerFile
	Log.fileHandle = file

	return l
}

func (l *logType) Print(logger *log.Logger, err interface{}, args ...interface{}) *logType {
	stack := ""
	if Conf.Log.ShowStack {
		prefix := logger.Prefix()
		if prefix == "FATAL: " || prefix == "ERROR: " || prefix == "DEBUG: " {
			stackBytes := make([]byte, 10240)
			runtime.Stack(stackBytes, false)
			stack = "\n" + string(stackBytes[:bytes.Index(stackBytes, []byte("\x00"))]) + "\n"
		}
	}

	if e, ok := err.(*Error); ok {
		logger.Printf("Error in %s (line %d): %s%s", e.Path, e.Line, e.Description, stack)
	} else if e, ok := err.(string); ok {
		logger.Printf(e+stack, args...)
	} else if e, ok := err.(error); ok {
		logger.Print(e.Error() + stack)
	}

	return l
}

type loggerType struct {
	Fatal, Error, Warning, Info, Debug *log.Logger
}

// Fatal tests for the presence of an error. If an error is found, it makes a
// fatal call to the log, which logs the error and then kills the server. If
// the error requires special formatting, you may pass the arguments for the
// log.Printf function following the error. These will be used to generate a
// custom error message in the event of an error.
func Fatal(e error, msg ...interface{}) {
	if e != nil {
		if len(msg) > 0 {
			Log.Fatal(msg[0], msg[1:]...)
		}
		Log.Fatal(e)
	}
}

var (
	// Log enables access to several convenience loggers included in addition
	// to the built in log.
	Log *logType

	// Logger allows direct access to various loggers, otherwise accessed
	// through Log methods.
	Logger *loggerType
)

func init() {
	Log = &logType{}
	Log.Init()
}
