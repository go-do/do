package do

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"
)

var (
	// BytePool is a special sync pool whose members are byte buffers. It reuses existing
	// byte buffers rather than creating new ones, which is better on system resources, and
	// provides an efficient platform for otherwise expensive string operations.
	BytePool = &bytePool{
		sync.Pool{
			New: func() interface{} {
				return &byteBuffer{}
			},
		},
	}

	// REMOVE this once Bytepool has been verified to work
	bufferPool = &sync.Pool{
		New: func() interface{} {
			return &bytes.Buffer{}
		},
	}

	// Context that will be global to all requests.
	contextMap Map
)

// bytePool represents a sync pool of byte buffers realized in BytePool.
type bytePool struct {
	sync.Pool
}

// Get returns a byte buffer from the byte pool.
func (bp *bytePool) Get() *byteBuffer {
	bb := bp.Pool.Get().(*byteBuffer)
	bb.bytePool = bp
	return bb
}

// Flush resets the provided byte buffer, puts it back into the byte buffer pool, and returns
// the bytes that were in the buffer.
// func (bp *bytePool) Flush(bb *bytes.Buffer) []byte {
// 	output := bb.Bytes()
// 	bb.Reset()
// 	bp.Put(bb)
// 	return output
// }

type byteBuffer struct {
	bytes.Buffer
	bytePool *bytePool
}

// Flush resets this byte buffer, puts it back into the byte buffer pool from which it was
// taken, and returns the bytes that were in the buffer.
func (bb *byteBuffer) Flush() []byte {
	output := bb.Bytes()
	bb.Reset()
	bb.bytePool.Put(bb)
	bb.bytePool = nil
	return output
}

// request adds additional fields and functionality to the request object received by the
// handler function.
type request struct {
	*http.Request
	Route                        *routeMatch
	Protocol, ProtoHost, FullURL string
	contextMap                   Map
}

// View is used with views to pass various data to a view function, including the handler
// function's request and writer objects, context data, etc.
type View struct {
	context Map
	Request *request
	Writer  *bytes.Buffer
}

// Context merges the provided maps into this ViewData objects's context map. Any context
// provided in this manner will live in this object's context map for the life of the
// request and need not be provided again in future calls. The following are automatically
// included in each context map in addition to global context:
//
// 		Param: a shortcut to the request's parameter map, also at Request.Route.ParamMap
// 		Request: a reference to the request
func (v *View) Context(args ...Map) Map {
	for _, arg := range args {
		for key, value := range arg {
			v.context[key] = value
		}
	}

	return v.context
}

// Page creates a Page object in this view's context and returns a reference to it. Views
// that employ this method can use the page object to consolidate common page-related info
// in a single place. The Page object's Head and Foot methods can then be used within a
// template to stitch everything together, or fields may be manipulated individually. See
// the Page type for more.
func (v *View) Page() *Page {
	thisPage := &Page{
		CssFiles:  css[:],
		HtmlFrags: []string{},
		JsFiles:   js[:],
		MetaTags:  SMap{},
		Scripts:   []string{},
		Styles:    []string{},
	}
	v.context["Page"] = thisPage
	return thisPage
}

// ThisContext copies the current View's context map and merges any additional conext maps
// provided as arguments into this copy. Any context provided in this manner will not affect
// the View object's context map and thus will not appear in future calls to Context or
// ThisContext.
func (v *View) ThisContext(args ...Map) Map {
	context := make(Map)
	for key, value := range v.context {
		context[key] = value
	}

	for _, arg := range args {
		for key, value := range arg {
			context[key] = value
		}
	}

	return context
}

// Start initializes the framework and the server and should be the last call in your
// main function.
func Start() {
	requestHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		Log.Init()

		// Build a new request object
		protocol := "http"
		if r.TLS != nil {
			protocol = "https"
		}
		protoHost := protocol + "://" + r.Host
		fullURL := protoHost + r.URL.Path
		newRequest := request{
			r,
			&routeMatch{},
			protocol,
			protoHost,
			fullURL,
			make(Map),
		}

		// Copy over global context and add the request to it
		context := Map{}
		for k, v := range contextMap {
			context[k] = v
		}
		context["Request"] = &newRequest

		// Use a buffer pool to control output
		bp := bufferPool.Get().(*bytes.Buffer)

		// This object contains everything we need for our views
		view := View{context, &newRequest, bp}

		defer func() {
			e := recover()
			if e == nil {
				// No errors; write our output buffer to the response
				bp.WriteTo(w)
			} else if Conf.Debug {
				// Display error info on screen
				showError(w, bp, newRequest, e)
			} else {
				// Log the error
				Log.Error(e)
			}

			// Clear the buffer and return it to the pool
			bp.Reset()
			bufferPool.Put(bp)
		}()

		// Retrive request data
		if newRequest.Header.Get("Content-Type") == "multipart/form-data" {
			Err(newRequest.ParseMultipartForm(10485760))
		} else {
			Err(newRequest.ParseForm())
		}

		// Determine which route, if any, matches the request
		noMatch := true
		for _, name := range Routes.Order {
			route := Routes.Map[name]
			if match := route.Pattern.FindStringSubmatch(r.URL.Path); len(match) > 0 {
				paramMap := make(map[string]string)
				if len(route.Params) == len(match)-1 {
					// We have a 1:1 correspondence between parameters and capturing groups
					noMatch = false
					for i, param := range route.Params {
						paramMap[param] = match[i+1]
					}
					newRequest.Route = &routeMatch{&route, &paramMap}
					view.context["Param"] = &paramMap
					route.View(view)
					break
				} else {
					Log.Error("Route \"%v\" has non-parameter related capturing groups; this is not allowed", route.Name)
					// Load 500 error page
				}
			}
		}

		if noMatch {
			fmt.Println("NO MATCH")
			// Load 400 error page
		}
	})

	// Last things before serving
	Conf.TemplatePaths = append(Conf.TemplatePaths, Conf.Path["do.template"])

	server := &http.Server{
		Addr:           ":9999",
		Handler:        requestHandler,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	for _, name := range Routes.Order {
		fmt.Println(Routes.Map[name])
	}

	log.Println("Now serving!")
	log.Fatal(server.ListenAndServe())
}
