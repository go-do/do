package do

import (
	"encoding/binary"
	"math"
	"reflect"
	"runtime"
	"strconv"
)

// ByteCount returns the size in bytes of the content provided for several of Go's basic
// types, plus the common types of []byte, []string, and []int, as well as any value
// accepted by Go's binary.Size function. If a byte value is not available, it returns -1.
func ByteCount(content interface{}) int {
	switch v := content.(type) {
	case []byte:
		return len(v)
	case []string:
		sum := 0
		for _, each := range v {
			sum += len(each)
		}
		return sum
	case []int:
		return len(v) * 8
	case string:
		return len(v)
	case int, uint:
		return strconv.IntSize / 8
	case int64, uint64, float64, complex64:
		return 8
	case int32, uint32, float32:
		return 4
	case int16, uint16:
		return 2
	case bool, int8, uint8:
		return 1
	case complex128:
		return 16
	default:
		return binary.Size(v)
	}
}

// Reverse reverses the runes in a string or members of a slice
func Reverse(obj interface{}) interface{} {
	v := reflect.ValueOf(obj)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	switch v.Kind() {
	case reflect.String:
		s := []rune(v.String())
		l := len(s)
		n := make([]rune, l)
		for i := 0; i < l; i++ {
			n[l-1-i] = s[i]
		}
		return string(n)

	case reflect.Slice:
		l := v.Len()
		n := reflect.MakeSlice(v.Type(), l, l)
		for i := 0; i < l; i++ {
			n.Index(l - 1 - i).Set(v.Index(i))
		}
		return n.Interface()
	}

	return obj
}

// Rotate rotates the given string or slice by rotateBy runes or indices.
// Negative rotates left, positive right. If rotateBy is larger than the
// length of obj, obj will be rotated left or right by the remainder of
// rotateBy divided by the length.
func Rotate(obj interface{}, rotateBy int) interface{} {
	return obj
}

// Round takes a floating point number and rounds it to the given precision. A negative
// prescision will result in the whole portion of the number being rounded. For example,
// Round(125.34783, -2) will yield 100, whereas Round(125.34783, 2) will yield 125.35.
func Round(x float64, p int) float64 {
	power := math.Pow10(p)
	whole, decimal := math.Modf(x * power)
	if decimal >= 0.5 {
		whole++
	}
	return float64(whole) / power
}

// FuncName returns the name of an arbirtrary function
func FuncName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}
