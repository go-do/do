package do

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
	"time"
)

var (
	baseSize int64
)

// func myCallback() interface{} {
// 	return "GENERATED"
// }

func initCache() {
	Register.Cache(DurationMap{
		"one": time.Minute,
		"two": time.Minute * 2,
	}, DurationMap{
		"group": time.Minute * 4,
	})
	baseSize = <-Cache.size
	Cache.size <- baseSize
}

func Test_ItemGet_NoContent(t *testing.T) {
	var result interface{}
	initCache()

	result = Cache.Get("one")
	if result != nil {
		t.Error("Expecting nil, got", result)
	}
}

// func Test_ItemSet_DefaultDuration(t *testing.T) {
// 	Cache.Set("one", "ENTRY")
// 	item := Cache.items["one"].item
// 	if content, ok := item.content.(string); !ok || content != "ENTRY" {
// 		t.Error("Expecting \"ENTRY\", got", content)
// 	} else if item.duration != 60 {
// 		t.Error("Expecting 60, got", item.duration)
// 	} else if item.size != 5 {
// 		t.Error("Expecting 5, got", item.size)
// 	}
// 	Cache.items["one"].item = cacheItemInfo{}
// }

// func Test_ItemSet_OtherDuration(t *testing.T) {
// 	Cache.Set("one", "ENTRY2", time.Minute*2)
// 	item := Cache.items["one"].item
// 	if content, ok := item.content.(string); !ok || content != "ENTRY2" {
// 		t.Error("Expecting \"ENTRY2\", got", content)
// 	} else if item.duration != 120 {
// 		t.Error("Expecting 120, got", item.duration)
// 	} else if item.size != 6 {
// 		t.Error("Expecting 6, got", item.size)
// 	}
// }

// func Test_ItemGet_ExistingValue(t *testing.T) {
// 	result, ok := Cache.Get("one").(string)
// 	if !ok || result != "ENTRY2" {
// 		t.Error("Expecting \"ENTRY2\", got", result)
// 	}
// }

// func Test_ItemContent_Unexpired(t *testing.T) {
// 	result := Cache.Content("one", "NEW ENTRY").(string)
// 	item := &Cache.items["one"].item
// 	if content, ok := item.content.(string); !ok || content != "ENTRY2" {
// 		t.Error("Expecting \"ENTRY2\", got", content)
// 	} else if content != result {
// 		t.Error("Expecting returned content to equal stored content, got", result, content)
// 	}

// 	// Make the item expired
// 	item.expires -= 300
// }

// func Test_ItemContent_Expired_Value(t *testing.T) {
// 	result := Cache.Content("one", "NEW ENTRY").(string)
// 	item := &Cache.items["one"].item
// 	if content, ok := item.content.(string); !ok || content != "NEW ENTRY" {
// 		t.Error("Expecting \"NEW ENTRY\", got", content)
// 	} else if content != result {
// 		t.Error("Expecting returned content to equal stored content, got", result, content)
// 	}

// 	// Make the item expired
// 	item.expires -= 300
// }

// func Test_ItemContent_Expired_Function(t *testing.T) {
// 	result := Cache.Content("one", myCallback).(string)
// 	item := &Cache.items["one"].item
// 	if content, ok := item.content.(string); !ok || content != "GENERATED" {
// 		t.Error("Expecting \"GENERATED\", got", content)
// 	} else if content != result {
// 		t.Error("Expecting returned content to equal stored content, got", result, content)
// 	}
// }

// func Test_ItemDelete(t *testing.T) {
// 	Cache.Delete("one", "two")
// 	item := &Cache.items["one"].item
// 	if item.content != nil {
// 		t.Error("Expecting nil content, got", item.content)
// 	}
// }

// func Test_GroupItemGet_NoContent(t *testing.T) {
// 	result := Cache.Group("group").Get("one")
// 	if result != nil {
// 		t.Error("Expecting nil, got", result)
// 	}
// }

// func Test_GroupItemSet_DefaultDuration(t *testing.T) {
// 	Cache.Group("group").Set("one", "ENTRY")
// 	item, ok := Cache.groups["group"].items["one"]
// 	if !ok {
// 		t.Error("Expecting \"one\" to exist in \"group\", creation failed")
// 	} else if item.content != "ENTRY" {
// 		t.Error("Expecting \"ENTRY\", got", item.content)
// 	} else if item.duration != 240 {
// 		t.Error("Expecting 240, got", item.duration)
// 	} else if item.size != 5+Cache.infoSize+3 {
// 		t.Errorf("Expecting %d (info: %d, content: %d, key: %d), got %d", 5+Cache.infoSize+3, Cache.infoSize, 5, 3, item.size)
// 	}
// 	delete(Cache.groups["group"].items, "one")
// }

// func Test_GroupItemSet_OtherDuration(t *testing.T) {
// 	Cache.Group("group").Set("one", "ENTRY2", time.Minute*2)
// 	item, ok := Cache.groups["group"].items["one"]
// 	if !ok {
// 		t.Error("Expecting \"one\" to exist, creation failed")
// 	} else if item.content != "ENTRY2" {
// 		t.Error("Expecting \"ENTRY2\", got", item.content)
// 	} else if item.duration != 120 {
// 		t.Error("Expecting 120, got", item.duration)
// 	} else if item.size != 6+Cache.infoSize+3 {
// 		t.Errorf("Expecting %d (info: %d, content: %d, key: %d), got %d", 6+Cache.infoSize+3, Cache.infoSize, 6, 3, item.size)
// 	}
// }

// func Test_GroupItemGet_ExistingValue(t *testing.T) {
// 	result, ok := Cache.Group("group").Get("one").(string)
// 	if !ok || result != "ENTRY2" {
// 		t.Error("Expecting \"ENTRY2\", got", result)
// 	}
// }

// func Test_GroupItemContent_Unexpired(t *testing.T) {
// 	result := Cache.Group("group").Content("one", "NEW ENTRY").(string)
// 	item := Cache.groups["group"].items["one"]
// 	if content, ok := item.content.(string); !ok || content != "ENTRY2" {
// 		t.Error("Expecting \"ENTRY2\", got", content)
// 	} else if content != result {
// 		t.Error("Expecting returned content to equal stored content, got", result, content)
// 	}

// 	// Make the item expired
// 	item.expires -= 300
// }

// func Test_GroupItemContent_Expired_Value(t *testing.T) {
// 	result := Cache.Group("group").Content("one", "NEW ENTRY").(string)
// 	item := Cache.groups["group"].items["one"]
// 	if content, ok := item.content.(string); !ok || content != "NEW ENTRY" {
// 		t.Error("Expecting \"NEW ENTRY\", got", content)
// 	} else if content != result {
// 		t.Error("Expecting returned content to equal stored content, got", result, content)
// 	}

// 	// Make the item expired
// 	item.expires -= 300
// }

// func Test_GroupItemContent_Expired_Function(t *testing.T) {
// 	result := Cache.Group("group").Content("one", myCallback).(string)
// 	item := Cache.groups["group"].items["one"]
// 	if content, ok := item.content.(string); !ok || content != "GENERATED" {
// 		t.Error("Expecting \"GENERATED\", got", content)
// 	} else if content != result {
// 		t.Error("Expecting returned content to equal stored content, got", result, content)
// 	}
// }

// func Test_GroupItemDelete(t *testing.T) {
// 	Cache.Group("group").Delete("one")
// 	item, ok := Cache.groups["group"].items["one"]
// 	if ok {
// 		t.Error("Expecting nil content, got", item.content)
// 	}
// }

// func Test_GroupItemClean(t *testing.T) {
// 	group := Cache.Group("group")

// 	// Create some items and make them expired
// 	for i := 0; i < 10; i++ {
// 		key := "key" + strconv.Itoa(i)
// 		group.Set(key, "CONTENT"+strconv.Itoa(i))
// 		Cache.groups["group"].items[key].expires -= 500
// 	}

// 	group.Clean()
// 	if len(group.items) > 0 {
// 		t.Error("Expecting all members removed, got member count", len(group.items))
// 	}
// }

// func Test_GroupItemEuthanize(t *testing.T) {
// 	group := Cache.Group("group")

// 	// Create some items; make some expired, others about to expire, and others safe
// 	for i := 10; i <= 100; i += 10 {
// 		key := "key" + strconv.Itoa(i)
// 		group.Set(key, "CONTENT"+strconv.Itoa(i), time.Second*time.Duration(i))
// 		Cache.groups["group"].items[key].expires -= 30
// 	}

// 	group.Euthanize()
// 	now, killAfter := time.Now().Unix(), int64(100-Conf.Cache.EuthanizeAfter)
// 	for _, item := range group.items {
// 		percentRemaining := (item.expires - now) * 100 / item.duration
// 		if percentRemaining < killAfter {
// 			t.Errorf("Expecting this member to be removed after %d%% of its life, got member at %d%% of its life remaining",
// 				Conf.Cache.EuthanizeAfter, percentRemaining)
// 		}
// 	}
// }

// func Test_GroupItemClear(t *testing.T) {
// 	group := Cache.Group("group")
// 	group.Clear()
// 	if len(group.items) > 0 {
// 		t.Error("Expecting no items in group, got", len(group.items))
// 	} else if group.size != 0 {
// 		t.Error("Expecting group size of 0, got", group.size)
// 	}
// }

// func Test_CacheClear(t *testing.T) {
// 	Cache.Set("one", "ENTRY1")
// 	Cache.Set("two", "ENTRY2")
// 	Cache.Group("group").Set("arbitrary", "SOME VALUE")

// 	// Pause to let hitSet finish
// 	time.Sleep(time.Nanosecond * 500)

// 	Cache.Clear()
// 	if Cache.items["one"].item.size > 0 || Cache.items["two"].item.size > 0 {
// 		t.Error("Expecting items to be empty, got non-empty items")
// 	} else if len(Cache.groups["group"].items) > 0 {
// 		t.Error("Expecting \"group\" to have zero items, got", len(Cache.groups["group"].items))
// 	}
// }

// func Test_CacheClean(t *testing.T) {
// 	// Create some items and expire one of them
// 	Cache.Set("one", "ENTRY1")
// 	Cache.Set("two", "ENTRY2")
// 	Cache.items["one"].item.expires -= 500

// 	// Create some items; make some expired and others safe
// 	group := Cache.Group("group")
// 	for i := 10; i <= 100; i += 10 {
// 		key := "key" + strconv.Itoa(i)
// 		group.Set(key, "CONTENT"+strconv.Itoa(i), time.Second*time.Duration(i))
// 		Cache.groups["group"].items[key].expires -= 35
// 	}

// 	Cache.Clean()

// 	// Pause for the process to complete
// 	time.Sleep(time.Nanosecond * 500)

// 	if Cache.items["one"].item.size != 0 {
// 		t.Error("Expecting \"one\" to be expired, got unexpired")
// 	} else if Cache.items["two"].item.size == 0 {
// 		t.Error("Expecting \"two\" to be unexpired, got expired")
// 	} else if len(Cache.groups["group"].items) > 7 {
// 		t.Error("Expecting 7 remaining items in \"group\", got", len(Cache.groups["group"].items))
// 	}
// }

// func Test_CacheEuthanize(t *testing.T) {
// 	// Create some items and set one near expiration
// 	Cache.Set("one", "ENTRY1")
// 	Cache.Set("two", "ENTRY2")
// 	Cache.items["one"].item.expires -= 50

// 	// Create some items; make some expired, others about to expire, and others safe
// 	group := Cache.Group("group")
// 	for i := 10; i <= 100; i += 10 {
// 		key := "key" + strconv.Itoa(i)
// 		group.Set(key, "CONTENT"+strconv.Itoa(i), time.Second*time.Duration(i))
// 		Cache.groups["group"].items[key].expires -= 35
// 	}

// 	Cache.Euthanize()

// 	// Pause for the process to complete
// 	time.Sleep(time.Second * 1)

// 	if Cache.items["one"].item.size != 0 {
// 		t.Error("Expecting \"one\" to be euthanized, got present")
// 	} else if Cache.items["two"].item.size == 0 {
// 		t.Error("Expecting \"two\" to be present, got euthanized")
// 	} else if len(Cache.groups["group"].items) != 6 {
// 		t.Error("Expecting 6 remaining items in \"group\", got", len(Cache.groups["group"].items))
// 	}
// }

func Test_CachePrune(t *testing.T) {
	// Should prune to 350000, given max of 500000
	Conf.Cache.MaxSize = 500000
	Conf.Cache.PruneAllowAfter = 0
	Cache.pruneTo = 350000

	// Reduce time constraints for testing
	Cache.pruneMinInterval = 0
	Cache.euthanizeMinInterval = 0
	Conf.Cache.PruneAllowAfter = 0

	content := "0123456789"

	// This one gets full life and 400 hits; popularity = 400 * 60 / 60
	Cache.Set("one", content)
	for i := 0; i < 200; i++ {
		Cache.command <- cacheCommand{do: cacheHit, key: "one"}
	}

	Cache.Set("two", content)
	for i := 0; i < 1000; i++ {
		Cache.command <- cacheCommand{do: cacheHit, key: "two"}
	}

	// Create some items; make some expired, others about to expire, and others safe
	group := Cache.Group("group")
	for i := 0; i < 300; i++ {
		key := "key" + strconv.Itoa(i)
		group.Set(key, strings.Repeat(content, i+1), time.Second*time.Duration(1+i))
		for j := 0; j < i; j++ {
			// As many hits for this item as its loop number
			Cache.command <- cacheCommand{do: cacheHit, key: key + ":group"}
		}
	}

	if Cache.Size() <= 350000 {
		t.Errorf("Expecting a starting cache size over 350000 bytes, got %d bytes", Cache.Size())
	}

	Cache.Prune()

	// Pause to let the pruning process finish
	time.Sleep(time.Second * 1)

	if Cache.Size() > 350000 {
		t.Errorf("Expecting an ending cache size under 350000 bytes, got %d bytes", Cache.Size())
	}
}

// func Test_CacheConcurrentOps(t *testing.T) {
// 	Cache.setMinInterval = 0
// 	Cache.Clear()
// 	fmt.Println("HERE")

// 	for i := 0; i < 500; i++ {
// 		go func(count int) {
// 			if count%2 == 0 {
// 				Cache.Set("one", 1)
// 				for j := 0; j < 50; j++ {
// 					Cache.Get("two")
// 				}
// 			} else {
// 				Cache.Set("two", 1)
// 				for j := 0; j < 50; j++ {
// 					Cache.Get("one")
// 				}
// 			}
// 			content := "0123456789"
// 			for j := 0; j < 100; j++ {
// 				group := Cache.Group("group")
// 				key := "key" + strconv.Itoa(count*j)
// 				group.Set(key, strings.Repeat(content, j+1), time.Second*time.Duration(1+j))
// 				for k := 0; k < j; k++ {
// 					// As many hits for this item as its loop number
// 					Cache.command <- cacheCommand{do: cacheHit, key: key + ":group"}
// 				}
// 			}
// 		}(i)
// 	}
// 	fmt.Println("HERE")

// 	time.Sleep(time.Second * 3)

// 	if Cache.Size() > 500000 {
// 		t.Errorf("Expecting an ending cache size under 500000 bytes, got %d bytes", Cache.Size())
// 	}
// }

func Test_CacheStats(t *testing.T) {
	summary := Cache.Stats()
	fmt.Println(summary.Overall)
}

func TestItemGetNotFound(t *testing.T) {
	fmt.Println("blah"+strconv.Itoa(2), strings.Join([]string{"1", "2"}, ","))
}
