package do

import (
	// "fmt"
	"html/template"
	"io/ioutil"
	"os"
	"strings"
)

var (
	css      = []string{}          // Names of global css files to include on a page
	cssFiles = map[string]string{} // Global map of css file name to file content
	js       = []string{}          // Names of global js files to include on a page
)

func CssFile(filePath string, nodes ...interface{}) {
	out, p := []string{}, Conf.Path["css"]+filePath

	// Compressed or readable format
	bop, c, eol, eop := "{", ":", ";", "}"
	if !Conf.Debug {
		bop, c, eol, eop = " {\n\t", ": ", ";\n", ";\n}\n\n"
	}

	var getContent func(nodes []interface{}, selector string)
	getContent = func(nodes []interface{}, selector string) {
		var children []interface{}

		for _, node := range nodes {
			leaf, isLeaf := node.(CssLeaf)
			if !isLeaf {
				branch := node.(CssBranch)
				leaf = CssLeaf{branch.Selector, branch.Properties}
				children = branch.Nodes
			}
			newSelector := selector + leaf.Selector

			props := []string{}
			for prop, value := range leaf.Properties {
				props = append(props, prop+c+value)
			}
			out = append(out, newSelector+bop+strings.Join(props, eol)+eop)

			if len(children) > 0 {
				getContent(children, newSelector)
			}
		}
	}

	// Generate css file content
	getContent(nodes, "")

	// Write the file to the given path
	pParts := strings.Split(p, "/")
	Err(os.MkdirAll(strings.Join(pParts[:len(pParts)-1], "/")+"/", 0744))
	Err(ioutil.WriteFile(p+".css", []byte(strings.Join(out, "")), 0644))
}

func Css(selector string, properties SMap, nodes ...interface{}) interface{} {
	leaf := CssLeaf{selector, properties}
	if len(nodes) > 0 {
		return CssBranch{leaf, nodes}
	} else {
		return leaf
	}
}

// CssLeaf should contain a selector and the properties to be associated with matching elements
type CssLeaf struct {
	Selector   string
	Properties SMap
}

// CssBranch should be used when you want to extend upon a Selector to form more a more complex selector.
// Nodes may contain elements of type CssLeaf or CssBranch. For example, if we have a class "row" whose
// elements all need a 5px padding, and we want to add an "even" class to even rows and an "odd" class
// to odd rows for alternating background colors, we would need three selectors ".row", ".row.even",
// and ".row.odd". This could be written as follows using CssLeaf and CssBranch:
//
//		CssBranch{
//			".row", SMap{"padding":"5px"},
//			[]interface{}{
//				CssLeaf{".even", SMap{"background":"#EEE"}},
//				CssLeaf{".odd", SMap{"background":"#DDD"}},
//			},
//		}
//
// This will produce the following compressed css:
//
// 		.row{padding:5px}.row.even{background:#EEE}.row.odd{background:#DDD}
//
// Members of the Nodes interface array may be either of type CssBranch or CssLeaf. The former represents
// a leaf in the style tree, the latter a branch, allowing for more branches or leaves underneath. Note
// that selectors are strung together without spaces, so be sure to add these in the selectors where
// appropriate.
type CssBranch struct {
	CssLeaf
	Nodes []interface{}
}

// CssRoot provides a root from which to build a style tree using CssBranch and CssLeaf elements. It
// takes the name of the file to be created and a slice of elements that can either be of type
// CssBranch or CssLeaf. See the CssBranch method for details on how this tree is formed.
type CssRoot struct {
	Name  string
	Nodes []interface{}
}

// Write takes this css file, processes it into compressed css, and writes the file to the default
// css folder, specified in Conf.Path["css"] ("/static/css/" by default). To specify a path beyond
// this folder, provide a path as an argument. This will be added to the css path from Conf. If
// a readable file is needed for debugging, make sure that Conf.Debug is set to true.
func (c *CssRoot) Write(path ...string) {
	bb, p := BytePool.Get(), Conf.Path["css"]

	// Compressed or readable format
	f, b, m, l := "{", ":", ";", "}"
	if Conf.Debug {
		f, b, m, l = " {\n\t", ": ", ";\n", "}\n\n"
	}

	var getContent func(nodes []interface{}, selector string)
	getContent = func(nodes []interface{}, selector string) {
		var children []interface{}

		for _, node := range nodes {
			leaf, isLeaf := node.(CssLeaf)
			if !isLeaf {
				branch := node.(CssBranch)
				leaf = CssLeaf{branch.Selector, branch.Properties}
				children = branch.Nodes
			}
			newSelector := selector + leaf.Selector

			bb.WriteString(newSelector + f)
			for prop, value := range leaf.Properties {
				bb.WriteString(prop + b + value + m)
			}
			bb.WriteString(l)

			if len(children) > 0 {
				getContent(children, newSelector)
			}
		}
	}

	// Generate css file content
	getContent(c.Nodes, "")
	content := bb.Flush()

	// Write the file to the given path
	if len(path) > 0 {
		p += path[0]
	}
	Err(os.MkdirAll(p, 0744))
	Err(ioutil.WriteFile(p+c.Name+".css", content, 0644))
}

/*
Before server start, all css files are predefined and stored in compressed format in separate files,
according to the names provided. Every time the server is started, these files are rewritten with any
changes. These are the files that will be called upon.

Call Register.Css("common1", "common2"...) to register any css that should be applied to all views.
Call Css("one", "two") to add more css files to a page. These names will be added to a string slice in the
context map for this request with the key "Css". This key is automatically provided in any call to the
Context method of viewData.  The two calls above only create the list.  They do not actually include the
links in the html. This should be done explicitly in the template

When a view is called, any preregistered css files will automatically be included without further action.
To include more files for a particular view, use Css("one", "two"...).

Want to have a Css function that is callable from views


*/

// Page is a collection of common page elements. It is created and made available in a view's context when
// that view's Page method is called. Use it's various methods to populate it and then process it's members
// individually or use the Head and Foot methods to do so automatically within a template.
type Page struct {
	CssFiles  []string
	HtmlFrags []string
	JsFiles   []string
	MetaTags  SMap
	Scripts   []string
	Styles    []string
	Title     string
}

// New is a convenience method for setting title, keywords, description, css files, and js
// files to be included on this page in a single call. It returns a reference to the page
// object for further manipulation.
func (p *Page) New(title, keywords, description string, args ...[]string) *Page {
	l := len(args)
	p.Title = title
	if keywords != "" {
		p.MetaTags["keywords"] = keywords
	}
	if description != "" {
		p.MetaTags["description"] = description
	}
	if l > 0 {
		p.Css(args[0]...)
		if l > 1 {
			p.Js(args[1]...)
		}
	}

	return p
}

// Css adds css files to the list of css includes for this page. These are automatically
// assembled when executing the Head method.
func (p *Page) Css(name ...string) *Page {
	p.CssFiles = append(p.CssFiles, name...)
	return p
}

// Foot is a convenience method that is intended for placement at the bottom of a page.
// It assembles any trailing html present in HtmlFrags and then an scripts in Scripts and
// returns the result as HTML for insertion in a template.
func (p *Page) Foot() template.HTML {
	foot := BytePool.Get()

	if len(p.HtmlFrags) > 0 {
		foot.WriteString(strings.Join(p.HtmlFrags, ""))
	}
	for _, script := range p.Scripts {
		foot.WriteString("<script>" + script + "</script>")
	}

	return template.HTML(foot.Flush())
}

// Head is a convenience method that is intended for placement in the head tag of a page.
// It assembles and formats the page title, meta tag pairs in MetaTags, css files in CssFiles,
// js files in JsFiles, and styles in Styles.
func (p *Page) Head() template.HTML {
	head := BytePool.Get()

	head.WriteString("<title>" + p.Title + "</title>")
	for name, content := range p.MetaTags {
		head.WriteString("<meta name=\"" + name + "\" content=\"" + content + "\">")
	}
	for _, file := range p.CssFiles {
		head.WriteString("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + file + ".css\">")
	}
	for _, file := range p.JsFiles {
		head.WriteString("<script src=\"" + file + ".js\"></script>")
	}
	if len(p.Styles) > 0 {
		head.WriteString("<style>" + strings.Join(p.Styles, "") + "</style>")
	}

	return template.HTML(head.Flush())
}

// Html adds html fragments to HtmlFrags, a list of fragments intended for inclusion at the
// end of the page. These are automatically assembled when executing the Foot method.
func (p *Page) Html(frags ...string) *Page {
	p.HtmlFrags = append(p.HtmlFrags, frags...)
	return p
}

// Js adds javascript files to the list of script includes for this page. These are
// automatically assembled when executing the Head method.
func (p *Page) Js(name ...string) *Page {
	p.JsFiles = append(p.JsFiles, name...)
	return p
}

// Script adds to the list of scripts to include on this page. These are automatically
// assembled when executing the Foot method.
func (p *Page) Script(script ...string) *Page {
	p.Scripts = append(p.Scripts, script...)
	return p
}

// Style adds to the list of styles to include on this page. These are automatically
// assembled when executing the Head method.
func (p *Page) Style(css ...string) *Page {
	p.Styles = append(p.Styles, css...)
	return p
}

// Css allows for registration of the provided css files among all pages
func (r *register) Css(names ...string) {
	if !r.css {
		css = names
		r.css = true
	}
}

// Css allows for registration of the provided js files among all pages
func (r *register) Js(names ...string) {
	if !r.js {
		js = names
		r.js = true
	}
}
